<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" class="es-es">
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<meta http-equiv="imagetoolbar" content="false" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>STORMMANE | Servidor Privado</title>
	<link rel="icon" href="/assets/wow/static/images/icons/favicon.gif" type="image/x-icon" />
	<link rel="shortcut icon" href="/assets/wow/static/images/icons/favicon.gif" type="image/x-icon" />
	<link rel="search" type="application/opensearchdescription+xml" href="http://eu.battle.net/es-es/data/opensearch" title="Búsqueda eb Battle.net" />
	<script type="text/javascript" src="/assets/wow/static//local-common/js/third-party.js?v=58"></script>
	<script type="text/javascript" src="/assets/wow/static//local-common/js/common-game-site.min.js?v=58"></script>
	<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static//wow.min.1.css?v=58-127" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static///wow.wow.min.29.css?v=58-127" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/style.css" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="Legion" />
	<link rel="stylesheet" type="text/css" href="/assets/fontawesome-all.css">
	<script type="text/javascript">
		//<![CDATA[
		var Core = Core || {},
			Login = Login || {};
		Core.staticUrl = '/assets/wow/static/';
		Core.sharedStaticUrl = '/assets/wow/static/local-common';
		Core.baseUrl = '/assets/wow/static/es';
		Core.projectUrl = '/wow';
		Core.cdnUrl = '<?=base_url()?>';
		Core.supportUrl = '<?=base_url()?>';
		Core.secureSupportUrl = '<?=base_url()?>';
		Core.project = 'wow';
		Core.locale = 'es-es';
		Core.language = 'es';
		Core.region = 'eu';
		Core.shortDateFormat = 'dd/MM/yyyy';
		Core.dateTimeFormat = 'dd/MM/yyyy HH:mm';
		Core.loggedIn = false;
		Core.userAgent = 'web';
		Login.embeddedUrl = '<?=base_url()?>';
		var Flash = Flash || {};
		Flash.videoPlayer = '<?=base_url()?>//assets/wow/static//images/video-player.swf';
		Flash.videoBase = '<?=base_url()?>//assets/wow/static//images/';
		Flash.ratingImage = '<?=base_url()?>//assets/wow/static//images/es-es.jpg';
		Flash.expressInstall = '<?=base_url()?>//assets/wow/static//images/expressInstall.swf';
		var _gaq = _gaq || [];

		_gaq.push(['_setAccount', 'UA-544112-16']);

		_gaq.push(['_setDomainName', '.battle.net']);

		_gaq.push(['_setAllowLinker', true]);

		_gaq.push(['_trackPageview']);
		//]]>
	</script>

	<style>
		.uk-label {
			display: inline-block;
			padding: 0 10px;
			background: #1e87f0;
			line-height: 1.5;
			font-size: 15px;
			color: #fff;
			vertical-align: middle;
			white-space: nowrap;
			border-radius: 2px;
			text-transform: uppercase;
		}

		.uk-label-danger {
			background-color: #f0506e;
			color: #fff;
		}

		.uk-label-success {
			background-color: rgb(13, 226, 14);
			color: #fff;
		}

		.uk-border-circle {
			border-radius: 50%;
		}

		.not-active {
			pointer-events: none;
			cursor: default;
			text-decoration: none;
			color: black;
		}

	</style>

	<meta property="fb:app_id" content="175314325675" />
	<meta property="og:site_name" content="World of Warcraft" />
	<meta property="og:locale" content="es_ES" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="/assets/wow/static//images/og-wow.png" />
	<meta property="og:image" content="/assets/wow/static//images/og-blizzard.png" />
	<meta property="og:title" content="World of Warcraft" />
	<meta property="og:description" content="Legion - Legion - World of Warcraft" />
</head>
<body class="es-es page full-page-view">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<link rel="image_src" href="/assets/wow/static//images/og-wow.png" />
<meta property="og:site_name" content="World of Warcraft®" />
<meta property="og:title" content="Legion - Legion - World of Warcraft" />
<meta property="og:image" content="/assets/wow/static//images/og-wow.png" />
<meta property="og:url" content="http://eu.battle.net/wow/es/legion/" />
<meta property="og:type" content="website" />
<meta property="fb:admins" content="100001571535053" />
<script>
	//<![CDATA[
	var dataLayer = [{
		"authenticated": "0"
	}];
	(function(w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({
			"gtm.start": new Date().getTime(),
			event: "gtm.js"
		});
		var f = d.getElementsByTagName(s)[0],
			j = d.createElement(s),
			dl = l != "dataLayer" ? "&amp;l=" + l : "";
		j.async = true;
		j.src =
			"//www.googletagmanager.com/gtm.js?id=" + i + dl;
		f.parentNode.insertBefore(j, f);
	})
	(window, document, "script", "dataLayer", "GTM-M39SFZ");
	//]]>
</script>
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/local-common/css/common.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/navbar.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/legal/ratings.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/seven-oh/seven-oh.css?v=127" />
<div class="Navigation">
	<div class="Navigation-full">
		<div class="Navigation-fullWrap">
			<div class="VertAlign">
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--collapse">
						<ul class="Navigation-list Navigation-list--gameSite">
							<li class="Navigation-item">
								<a class="Navigation-link" href="" data-analytics="wow7-action" data-analytics-placement="Game Site">
									<span class="Navigation-label">FOROS</span>
									<span class="Navigation-icon Navigation-icon--gameSite"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--middle">
						<ul class="Navigation-list Navigation-list--buy">
							<li class="Navigation-item Navigation-item--hideInFull">
								<a class="Navigation-link" href="/descarga/WoW.rar" data-analytics="wow7-beta-sign-up" data-analytics-placement="Nav">
									<span class="Navigation-label">Descargar juego</span>
									<span class="Navigation-icon Navigation-icon--buy"></span>
								</a>
							</li>
							<li class="Navigation-item Navigation-item--hideInFull">
								<a onclick="return iniciarSession()" class="Navigation-link" href="#" data-analytics="wow7-buy" data-analytics-placement="Nav Button">
									<span class="Navigation-label">Jugar</span>
									<span class="Navigation-icon Navigation-icon--buy"></span>
								</a>
							</li>
						</ul>
						<ul class="Navigation-list Navigation-list--sections">
							<li class="Navigation-item Navigation-item--buyBox Navigation-item--hideInCompact">
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--collapse">
						<ul class="Navigation-list Navigation-list--social">
							<li class="Navigation-item">
								<a class="Navigation-link" href="https://www.facebook.com/stormmane/" data-slug="facebook" data-analytics="wow7-sns" data-analytics-placement="facebook - Nav">
									<div class="Navigation-label">Stormmane Facebook Oficial</div>
									<div class="Navigation-icon Navigation-icon--facebook"></div>
								</a>
							</li>
							<li class="Navigation-item">
								<a class="Navigation-link" href="https://twitter.com/stormmaneserver" data-slug="facebook" data-analytics="wow7-sns" data-analytics-placement="twitter - Nav">
									<div class="Navigation-label">Siguenos por Twitter</div>
									<div class="Navigation-icon Navigation-icon--twitter"></div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Navigation-compact">
		<div class="Navigation-compactToggle"></div>
		<div class="Navigation-compactWrap">
			<div class="Navigation-compactLogoWrap">
				<img class="Navigation-compactLogo" src="/assets/wow/static/images//seven-oh/logo.png" alt="World of Warcraft" />
			</div>
		</div>
		<div class="Navigation-compactShield"></div>
	</div>
</div>
<div class="Section Section--keyArtHeader VideoBackground VideoBackground--wide VideoBackground--responsiveBackdrop" id="home" data-nav="Inicio">
	<div class="VideoBackground-container">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/header-illidan-still.jpg" data-mp4="/assets/wow/static/images//header-illidan-still-mp4.jpg" />
	</div>
	<div class="Grid u-align-center" id="inicio">
		<h1 class="Logo">
			<img class="ScaledImage ScaledImage--logo" src="/assets/wow/static/images//seven-oh/logo.png" alt="World of Warcraft" />
		</h1>
		<div class="ButtonList ButtonList--keyArtHeader">
			<ul>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" onclick="$('#estado').modal('show');" href="#" data-analytics="wow7-beta-sign-up" data-analytics-placement="Top">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text">Est. Servidor</div>
					</a>
					<!--<a onclick="$('#estado').modal('show')" class="ButtonList-itemLabel ButtonList-itemLabel--link" href="#" data-analytics="wow7-action" data-analytics-placement="Starter Edition - Top">Estado Servidor</a>-->
				</li>
				<li class="ButtonList-item">
					<a onclick="return iniciarSession();" class="Button Button--fixedWidth" href="#" data-analytics="wow7-buy" data-analytics-placement="Top Button">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<?php
						if(isset($this->session->idUsuario)){
							echo '<div class="Button-text">Panel</div>';
						}else{
							echo '<div class="Button-text">Login</div>';
						}
						?>
					</a>
				</li>
			</ul>
		</div>
		<div class="InlineList">
			<div class="VideoThumbnail VideoThumbnail--large VideoThumbnail--keyArtHeaderCenter InlineList-item">
				<!-- here -->
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=9dtElNTvlN4" data-youtube-id="9dtElNTvlN4" data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images/RHJhZW5vblVwZ3JhZGUy.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images/RHJhZW5vblVwZ3JhZGUy.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="assets/wow/static/images/RHJhZW5vblVwZ3JhZGU.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Tráiler cinemático</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
			<div class="VideoThumbnail VideoThumbnail--keyArtHeaderLeft InlineList-item">
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=eTz5MO8XFyA" data-youtube-id="eTz5MO8XFyA" data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images/teaser.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images/teaser.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="/assets/wow/static/images/teaser-thumb.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Avance</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
			<div class="VideoThumbnail VideoThumbnail--keyArtHeaderRight InlineList-item">
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=jgkAVRwPV9I" data-youtube-id="jgkAVRwPV9I" data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images//announcement.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images//announcement.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="/assets/wow/static/images/announcement-thumb.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Vídeo de presentación</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="Grid u-align-center u-marginTop-xLarge u-zIndex-3">
		<div class="Body Body--keyArtHeader">
			<h1 class="Header--wideLetterSpacing">Bienvenidos a <br />STORMMANE</h1>
			<p>La Tumba de Sargeras se ha reabierto, y ahora los demonios de la Legión Ardiente entran a raudales en nuestro mundo. Están concentrando todo su terrorífico poder para invocar al titán oscuro en Azeroth… y ya han encontrado la clave para que regrese.</p>
			<p>Ahora que la Alianza y la Horda han sido vencidas, solo tú podrás empuñar los artefactos más legendarios de <em>Warcraft</em>, explorar las ancestrales Islas Abruptas en busca de reliquias de los titanes y enfrentarte a la Legión antes de que la última esperanza de Azeroth se desvanezca.</p>
			<p>Prepárate, adalid. La extinción se acerca.</p>
			<p/>
		</div>
	</div>
</div>
<div class="Section Section--bigDividerOne VideoBackground VideoBackground--tall VideoBackground--wide" id="class" data-nav="Clase héroe">
	<div class="VideoBackground-container u-tablet-hidden">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/demon-hunters-still.jpg" />
		<div class="VideoBackground-classTransition"></div>
	</div>
	<div class="Grid Grid--narrowGrid u-align-center" id="clases">
		<h2>Clase héroe</h2>
		<h1>Acecha a tus presas como cazador de demonios</h1>
		<p>Domina a tus adversarios como cazador de demonios, una clase que tiene sus orígenes en los elfos que fueron desterrados por atreverse a tomar los terribles poderes de la Legión. Los cazadores de demonios poseen una movilidad superior y un nivel sobrenatural de percepción, y pueden acceder a poderes prohibidos en momentos de necesidad para transformarse en seres aterradores. La especialización Caos te permitirá aniquilar a todo aquel que se cruce en tu camino con abrasadores ataques demoníacos, mientras que con la especialización Venganza podrás enfrentarte a los demonios más poderosos y someterte a un castigo infinito para que sus ataques exalten tu odio.</p>
	</div>
	<div class="PaginatorByIcon PaginatorByIcon--simple PaginatorByIcon--class u-desktop-hidden">
		<div class="PaginatorByIcon-iconsWrap">
			<ul class="PaginatorByIcon-icons"></ul>
		</div>
		<div class="PaginatorByIcon-content">
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/demon-hunter-female.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/demon-hunter-male.jpg" alt="" />
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
	<div class="MediaList Grid Grid--narrowGrid u-marginTop-xLarge">
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/spectral-sight-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Visión espectral</h3>
				<p class="Paragraph--small">
				<p>La aparente ceguera de los cazadores de demonios camufla sus verdaderos poderes de percepción. Hacen uso de su visión aumentada para detectar enemigos, aun si estos están ocultos detrás de obstáculos.</p>
				</p>
			</div>
		</div>
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/metamorphosis-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Metamorfosis</h3>
				<p class="Paragraph--small">Los cazadores de demonios se transforman en criaturas infernales para potenciar su especialización: los Illidari centrados en infligir daño obtienen velocidad y daño sobrenaturales, lo que les permite dar muerte rápida a sus presas; los que optan por un papel defensivo son prácticamente inmortales en su forma demoníaca.</p>
			</div>
		</div>
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/mobility-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Movilidad sin par</h3>
				<p class="Paragraph--small">
					<div dir="ltr" id="imcontent">
				<p><span dir="ltr">Los cazadores de demonios pueden realizar saltos dobles, irrumpir en los combates y huir de ellos, e incluso desplegar sus monstruosas alas para atacar y sorprender a los enemigos desde el cielo.</span></p>
			</div>
			</p>
		</div>
	</div>
</div>

<div class="Grid u-marginTop-xLarge">
	<div class="ThumbnailList">
		<ul class="ThumbnailList-items">
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-nelf-female-tank.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-nelf-female-tank.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-nelf-female-tank-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Venganza (Tanque)</span>
				</a>
			</li>
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-base.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-base.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-base-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Cazadores de demonios</span>
				</a>
			</li>
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-belf-male-dps.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-belf-male-dps.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-belf-male-dps-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Caos (DPS)</span>
				</a>
			</li>
		</ul>
	</div>
</div>
<div xmlns="http://www.w3.org/1999/xhtml" class="Grid u-align-center">
	<p>
		<a class="Button" href="/wow/game/class/demon-hunter" data-analytics="wow7-action" data-analytics-placement="Class Page">
		</a>
	</p>
</div>



<div class="Section Section--noPaddingBottom Section--bigDividerTwo" id="artifacts" data-nav="Artefactos">
	<div class="Grid u-align-center">
		<h2>Clase héroe</h2>
		<h1>Blande armas de un poder inimaginable</h1>
		<p>Solo los veteranos más curtidos de Azeroth poseen la fortaleza necesaria para blandir artefactos legendarios contra la Legión Ardiente. Tu arma mítica aumentará de nivel contigo, y tus elecciones modificarán sus facultades y su aspecto, su sonido y la sensación que transmite en el combate. Personaliza tu artefacto y conviértelo en el instrumento perfecto para la batalla; con él, guiarás a tu facción en las situaciones más desesperadas.</p>
	</div>
	<div class="PaginatorByIcon">
		<div class="PaginatorByIcon-iconsWrap">
			<div class="Grid">
				<ul class="PaginatorByIcon-icons"></ul>
			</div>
		</div>
		<div class="PaginatorByIcon-content VideoBackground VideoBackground--tall VideoBackground--wide" id="artefatos">
			<div class="VideoBackground-container"></div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="ashbringer" data-mp4-url="/assets/wow/static/images/ashbringer.mp4" data-webm-url="/assets/wow/static/images/ashbringer.webm" data-background-img-url="/assets/wow/static/images/ashbringer-still.jpg" data-responsive-img-url="/assets/wow/static/images/ashbringer-compact.png" data-thumbnail-url="/assets/wow/static/images/ashbringer-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/ashbringer-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Crematoria</h3>
							<h4 class="PaginatorByIcon-subheader">Espada de dos manos de paladín</h4>
							<p class="PaginatorByIcon-summary">Dicen que esta espada, forjada por Magni Barbabronce, confiere la gloria de la Luz a su portador y le permite reducir a sus enemigos a cenizas.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="maw-of-the-damned" data-mp4-url="/assets/wow/static/images/maw.mp4" data-webm-url="/assets/wow/static/images/maw.webm" data-background-img-url="/assets/wow/static/images/maw-still.jpg" data-responsive-img-url="/assets/wow/static/images/maw-compact.png" data-thumbnail-url="/assets/wow/static/images/maw-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/maw-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Fauce de los Malditos</h3>
							<h4 class="PaginatorByIcon-subheader">Hacha de dos manos de caballero de la Muerte</h4>
							<p class="PaginatorByIcon-summary">Durante eones, el demonio Sangralix el Desgarrador se sirvió de esta enorme hacha para robar la fuerza vital de sus enemigos y restablecer la suya propia. La Fauce desangra todo lo que toca. Solo sabe… engullir.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="staff-of-antonidas" data-mp4-url="/assets/wow/static/images/antonidas.mp4" data-webm-url="/assets/wow/static/images/antonidas.webm" data-background-img-url="/assets/wow/static/images/antonidas-still.jpg" data-responsive-img-url="/assets/wow/static/images/antonidas-compact.png" data-thumbnail-url="/assets/wow/static/images/antonidas-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/antonidas-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Ébano Glacial, Gran Bastón de Alodi</h3>
							<h4 class="PaginatorByIcon-subheader">Bastón de mago</h4>
							<p class="PaginatorByIcon-summary">El bastón del primer guardián de Tirisfal exuda una antinatural energía glacial, lo que permite a su portador mantener una mente fría, despierta y serena incluso en las situaciones más adversas.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="staff-of-shaohao" data-mp4-url="/assets/wow/static/images/shaohao.mp4" data-webm-url="/assets/wow/static/images/shaohao.webm" data-background-img-url="/assets/wow/static/images/shaohao-still.jpg" data-responsive-img-url="/assets/wow/static/images/shaohao-compact.png" data-thumbnail-url="/assets/wow/static/images/shaohao-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/shaohao-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Sheilun, Bastón de la Niebla</h3>
							<h4 class="PaginatorByIcon-subheader">Bastón de monje</h4>
							<p class="PaginatorByIcon-summary">El último emperador de Pandaria usó este legendario bastón para sumir su tierra en la niebla, y este reverbera con su eterno legado de sabiduría y perseverancia.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="windrunner" data-mp4-url="/assets/wow/static/images/windrunner.mp4" data-webm-url="/assets/wow/static/images/windrunner.webm" data-background-img-url="/assets/wow/static/images/windrunner-still.jpg" data-responsive-img-url="/assets/wow/static/images/windrunner-compact.png" data-thumbnail-url="/assets/wow/static/images/windrunner-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/windrunner-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Thas’dorah, Legado de los Brisaveloz</h3>
							<h4 class="PaginatorByIcon-subheader">Arco de cazador</h4>
							<p class="PaginatorByIcon-summary">Cuenta la leyenda que esta reliquia de familia élfica —otrora empuñada por un infame General Forestal de Lunargenta— puede convertir a un arquero mediocre en un tirador de élite… verdaderamente inigualable.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>

</div>
<div class="Section Section--noPadding Section--bigDividerOne VideoBackground VideoBackground--overlay" id="world" data-nav="Escenario">
	<div class="Grid tempClass-alignTop u-align-center">
		<h2>Escenario</h2>
		<h1>Explora las Islas Abruptas</h1>
	</div>
	<div class="VideoBackground-container u-tablet-hidden" id="escenarios">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/world-dissolve-still.jpg" />
		<div class="VideoBackground-overlay VideoBackground-overlay--dark">
			<div class="VideoBackground-overlaySize"></div>
		</div>
	</div>
	<div class="PaginatorByIcon PaginatorByIcon--simple u-desktop-hidden">
		<div class="PaginatorByIcon-iconsWrap">
			<ul class="PaginatorByIcon-icons"></ul>
		</div>
		<div class="PaginatorByIcon-content">
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/map-fullsize-esES.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-1.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-2.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-3.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-4.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-5.jpg" alt="" />
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
	<div class="Grid tempClass-alignBottom">
		<div class="Grid-cell Grid-cell--3of4 Grid-push-cell--1of4 Wide-cell--1of1">
			<p>Te aguardan las Islas Abruptas, epicentro de la invasión demoníaca y tierra repleta de maravillas ancestrales, con frondosos bosques, colosales sierras y ciudades de los elfos de la noche más antiguas que la civilización humana. No obstante, aquí también acecha el peligro: sátiros, drógbar y Kvaldir malditos vagan por las Islas junto al errante ejército de la Legión. Para superar estas amenazas, controlarás una sede única para la clase de tu personaje y dirigirás a tus seguidores en la búsqueda de los Pilares de la Creación, el secreto de la salvación de Azeroth.</p>
		</div>
		<div class="Grid-cell Grid-cell--1of4 Grid-pull-cell--3of4 Wide-cell--1of1 u-tablet-hidden">
			<div class="ThumbnailList">
				<ul class="ThumbnailList-items">
					<li class="ThumbnailList-item ThumbnailList-item--noBottomPadding">
						<a class="ThumbnailList-link Frame" href="/assets/wow/static/images/map-fullsize-esES.jpg" data-analytics="wow7-image" data-analytics-placement="Map" data-analytics-asset="/assets/wow/static/images/map-fullsize-esES.jpg">
							<div class="Frame-overlay"></div>
							<div class="Frame-side Frame-side--top"></div>
							<div class="Frame-side Frame-side--right"></div>
							<div class="Frame-side Frame-side--bottom"></div>
							<div class="Frame-side Frame-side--left"></div>
							<div class="Frame-corner Frame-corner--topLeft"></div>
							<div class="Frame-corner Frame-corner--topRight"></div>
							<div class="Frame-corner Frame-corner--bottomRight"></div>
							<div class="Frame-corner Frame-corner--bottomLeft"></div>
							<img class="ThumbnailList-image" src="/assets/wow/static/images/map-thumbnail.jpg" alt="" />
							<span class="ThumbnailList-label ThumbnailList-label--forFrame">Ver mapa</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="Section Section--noPaddingBottom Section--bigDividerTwo" id="characters" data-nav="Héroes y villanos">
	<div class="Grid">
		<h2>Héroes y villanos</h2>
		<h1>Un mundo dividido</h1>
	</div>
	<div class="PaginatorByIcon" id="heroesYvillanos">
		<div class="PaginatorByIcon-iconsWrap">
			<div class="Grid">
				<ul class="PaginatorByIcon-icons">
				</ul>
			</div>
		</div>
		<div class="PaginatorByIcon-content VideoBackground VideoBackground--tall VideoBackground--wide">
			<div class="VideoBackground-container"><video class="VideoBackground-video" loop="loop" poster="/assets/wow/static/images/maiev-still.jpg"><source src="/assets/wow/static/images/maiev.webm" type="video/webm"><source src="/assets/wow/static/images/maiev.mp4" type="video/mp4"></video></div>
			<div class="PaginatorByIcon-contentItem PaginatorByIcon-contentItem--selected" data-item-type="character" data-slug="maiev" data-mp4-url="/assets/wow/static/images/maiev.mp4" data-webm-url="/assets/wow/static/images/maiev.webm" data-background-img-url="/assets/wow/static/images/maiev-still.jpg" data-responsive-img-url="/assets/wow/static/images/maiev-compact.png" data-thumbnail-url="/assets/wow/static/images/maiev-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/maiev-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Maiev Cantosombrío</h3>
							<p class="PaginatorByIcon-summary">Si los Vigías son infames entre los elfos de la noche, Maiev Cantosombrío es una leyenda entre ellos. Famosa por su extraordinaria habilidad para capturar cualquier tipo de enemigo, siguió el rastro de Illidan Tempestira, el Traidor, hasta el Templo Oscuro, en Terrallende. Tras la derrota de Illidan, Maiev apresó a los seguidores que sobrevivieron, los Illidari, y juró que nunca permitiría que los vilificados cazadores de demonios volvieran a campar a sus anchas.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="xavius" data-mp4-url="/assets/wow/static/images/xavius.mp4" data-webm-url="/assets/wow/static/images/xavius.webm" data-background-img-url="/assets/wow/static/images/xavius-still.jpg" data-responsive-img-url="/assets/wow/static/images/xavius-compact.png" data-thumbnail-url="/assets/wow/static/images/xavius-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/xavius-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Xavius</h3>
							<p class="PaginatorByIcon-summary">Tiempo ha, Xavius aterrorizó Azeroth de tal manera que casi hizo añicos los muros que separan la realidad de los sueños. Sus maquinaciones terminaron con su derrota, pero se le ha dado otra oportunidad para vengarse. Ahora, Xavius lidera la conquista de Val’sharah, donde el contaminado Árbol del Mundo Shaladrassil extiende la corrupción de la Pesadilla Esmeralda. Con un ejército de sátiros viles a sus órdenes, Xavius no parará hasta aplastar a todo aquel que se oponga a la Legión.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="anduin" data-mp4-url="/assets/wow/static/images/anduin.mp4" data-webm-url="/assets/wow/static/images/anduin.webm" data-background-img-url="/assets/wow/static/images/anduin-still.jpg" data-responsive-img-url="/assets/wow/static/images/anduin-compact.png" data-thumbnail-url="/assets/wow/static/images/anduin-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/anduin-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Anduin Wrynn</h3>
							<p class="PaginatorByIcon-summary">Anduin, heredero del trono de Ventormenta, es un joven muy sabio para su edad. Son varias las veces que ha recurrido a la diplomacia para poner fin a los conflictos, y ha inspirado hasta a su aguerrido padre para deponer las armas en aras de la paz. No obstante, la diplomacia tiene un límite, y hay villanos con los que no se puede razonar. La Legión Ardiente amenaza con aniquilar Azeroth, y Anduin descubrirá el auténtico precio de la paz… y si está dispuesto a pagarlo.</p>

						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="athissa" data-mp4-url="/assets/wow/static/images/athissa.mp4" data-webm-url="/assets/wow/static/images/athissa.webm" data-background-img-url="/assets/wow/static/images/athissa-still.jpg" data-responsive-img-url="/assets/wow/static/images/athissa-compact.png" data-thumbnail-url="/assets/wow/static/images/athissa-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/athissa-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Maestra de las mareas Athissa</h3>
							<p class="PaginatorByIcon-summary">La reina Azshara, antigua líder de los ancestrales elfos de la noche, gobierna a los naga con autoridad suprema. Sus siervos se cuentan por millares, pero, de entre ellos, pocos son tan fanáticos como Athissa. Su Reina la designó para dirigir un enorme ejército naga hacia Azsuna, donde se rumorea que hay una reliquia de poder de los titanes, largo tiempo perdida. Nada se interpondrá entre Athissa y su objetivo, ni los espíritus malditos de los elfos de la noche que deambulan por esa región ni las tropas de la Horda y la Alianza que se concentran en las Islas Abruptas.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="genn" data-mp4-url="/assets/wow/static/images/genn.mp4" data-webm-url="/assets/wow/static/images/genn.webm" data-background-img-url="/assets/wow/static/images/genn-still.jpg" data-responsive-img-url="/assets/wow/static/images/genn-compact.png" data-thumbnail-url="/assets/wow/static/images/genn-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/genn-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Genn Cringris</h3>
							<p class="PaginatorByIcon-summary">Durante los últimos años, el veterano gobernador de Gilneas ha sufrido muchas desgracias: su hijo y su nación cayeron ante la Reina alma en pena Sylvanas y los Renegados, y casi perdió su humanidad a causa de la maldición huargen. Sin embargo, a pesar de todas estas tragedias, Genn ha hallado fuerza y coraje, y está decidido a luchar con uñas y dientes contra cualquier enemigo que amenace a la Alianza.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="dargrul" data-mp4-url="/assets/wow/static/images/dargrul.mp4" data-webm-url="/assets/wow/static/images/dargrul.webm" data-background-img-url="/assets/wow/static/images/dargrul-still.jpg" data-responsive-img-url="/assets/wow/static/images/dargrul-compact.png" data-thumbnail-url="/assets/wow/static/images/dargrul-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/dargrul-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Dargrul el Infrarrey</h3>
							<p class="PaginatorByIcon-summary">Durante siglos, las tribus tauren y drógbar de Monte Alto vivieron en paz, pero poco después de la llegada de la Legión, un líder drógbar llamado Dargrul el Infrarrey acabó con la armonía y robó el Martillo de Khaz’goroth, un poderoso artefacto protegido por los tauren Monte Alto. El Infrarrey piensa servirse del terrible poder del Martillo para tomar el control de Monte Alto.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="khadgar" data-mp4-url="/assets/wow/static/images/khadgar.mp4" data-webm-url="/assets/wow/static/images/khadgar.webm" data-background-img-url="/assets/wow/static/images/khadgar-still.jpg" data-responsive-img-url="/assets/wow/static/images/khadgar-compact.png" data-thumbnail-url="/assets/wow/static/images/khadgar-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/khadgar-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Khadgar</h3>
							<p class="PaginatorByIcon-summary">A pesar de que no pudo impedir que Gul’dan abriera la Tumba de Sargeras y la Legión volviera a irrumpir en Azeroth, Khadgar está decidido a hallar el modo de volver a sellar el portal y frustrar la invasión de la Legión. Sabe que, para conseguirlo, necesitará unir a los mayores campeones de Azeroth bajo la causa y hacer frente a aquellos que se oponen a formar una coalición, arriesgándose a acabar destruidos a manos de los demonios.</p>

						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="guldan" data-mp4-url="/assets/wow/static/images/guldan.mp4" data-webm-url="/assets/wow/static/images/guldan.webm" data-background-img-url="/assets/wow/static/images/guldan-still.jpg" data-responsive-img-url="/assets/wow/static/images/guldan-compact.png" data-thumbnail-url="/assets/wow/static/images/guldan-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/guldan-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Gul'dan</h3>
							<p class="PaginatorByIcon-summary">Gul'dan no debe lealtad a nadie salvo a sus amos de la Legión Ardiente. En Draenor, el ambicioso brujo orco estuvo a punto de someter a toda su raza bajo el yugo de los demonios. Aunque sus planes fracasaron, Gul'dan sobrevivió, y la Legión lo exilió a Azeroth, desde donde abriría un portal que permitiría la entrada de un monstruoso ejército invasor, algo a lo que ni la Horda ni la Alianza se han enfrentado jamás.</p>

						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="sylvanas" data-mp4-url="/assets/wow/static/images/sylvanas.mp4" data-webm-url="/assets/wow/static/images/sylvanas.webm" data-background-img-url="/assets/wow/static/images/sylvanas-still.jpg" data-responsive-img-url="/assets/wow/static/images/sylvanas-compact.png" data-thumbnail-url="/assets/wow/static/images/sylvanas-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/sylvanas-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Sylvanas Brisaveloz</h3>
							<p class="PaginatorByIcon-summary">La despiadada líder de los Renegados es una formidable campeona de su pueblo, pero, con la invasión de la Legión Ardiente, la Dama Oscura corre un riesgo extremo: si Sylvanas cae, su muerte será el comienzo de su eterna condena. Lo único que la separa de su fatal destino son sus Val'kyr, aunque son pocos los espíritus guardianes que quedan para apoyarla. Su destino pende de un hilo, y Sylvanas debe decidir hasta qué punto está dispuesta a proteger a su pueblo… y qué es lo más valioso para ella: su gente o su propia alma.</p>

						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="illidan" data-mp4-url="/assets/wow/static/images/illidan.mp4" data-webm-url="/assets/wow/static/images/illidan.webm" data-background-img-url="/assets/wow/static/images/illidan-still.jpg" data-responsive-img-url="/assets/wow/static/images/illidan-compact.png" data-thumbnail-url="/assets/wow/static/images/illidan-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/illidan-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Illidan Tempestira</h3>
							<p class="PaginatorByIcon-summary">Tras la caída del Templo Oscuro, el cadáver de Illidan Tempestira, el señor de Terrallende, desapareció. Nadie sabe qué fue de los restos de Illidan, pero la leyenda dice que la celadora Maiev Cantosombrío trasladó su cuerpo destrozado a la Cámara de los Celadores para que el alma oscura y persistente de Illidan sufriera el resto de su eterna sentencia, junto a sus seguidores, los temibles Illidari. Una justicia… perpetua.</p>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
</div>





<!-- no tocar -->
<div class="Section Section--bigDividerOne" id="media" data-nav="Medios">
	<script type="text/javascript">
		//<![CDATA[
		var mediaStrings = {
			filters: {
				artwork: "Ilustraciones",
				audio: "Audio",
				comics: "Cómics",
				featured: "Destacados",
				screenshots: "Capturas de pantalla",
				videos: "Vídeos",
				wallpapers: "Fondos de pantalla"
			},
			types: {
				artwork: "Ilustración",
				audio: "Audio",
				comic: "Cómic",
				screenshot: "Captura de pantalla",
				video: "Vídeo",
				wallpaper: "Fondo de pantalla"
			},
			sizes: {
				wide: "Grande",
				standard: "Normal",
				mobile: "Móvil",
				tablet: "Tableta",
				facebook: "Facebook",
				twitter: "Twitter"
			}
		};
		//]]>
	</script>

	<div data-media="">
	</div>

</div>
<!-- no tocar -->

<div class="Section Section--bigDividerTwo" id="features" data-nav="Descargar Cliente + Patch">
	<div class="Grid u-align-center" id="descargarCliente">
		<h2>Descargar Cliente + Parche</h2>
		<h1>La Legión Ardiente aguarda</h1>
		<p>A continuación se indican los requisitos mínimos para jugar World of Warcraft® y la expansión Legion 7.3.5 tanto en Windows® como en Mac®. Debido a posibles cambios en la programación, es posible que los requisitos mínimos de World of Warcraft cambien con el tiempo.</p>
	</div>
	<div class="Grid u-marginTop-none">
		<div class="Grid-cell Grid-cell--5of8 Grid-push-cell--3of8 Wide-cell--1of1 u-marginTop-medium">
			<img src="/assets/wow/static/images/legion-box-art.png" alt="" />
		</div>
		<div class="Grid-cell Grid-cell--3of8 Grid-pull-cell--5of8 Wide-cell--1of1 u-marginTop-medium FeaturesListContainer">
			<h3>Windows:</h3>
			<ul>
				<li>Windows XP / Windows Vista / Windows 7 / Windows 8 / Windows 10 con el último SP</li>
				<li>Intel Core2 Duo E8500 o AMD Phenom II X3 720</li>
				<li>NVIDIA GeForce GT 440 – AMD Radeon HD 5670 – Intel HD Graphics 5000</li>
				<li>RAM: 2GB</li>
				<li>D.D: 45GB</li>
			</ul>
			<h3>Mac:</h3>
			<ul>
				<li>OS X 10.10 (última versión)</li>
				<li>Intel Core i5-750</li>
				<li>NVIDIA GeForce GT 640M – ATI Radeon HD 4850 – Intel HD Graphics 5000</li>
				<li>RAM: 4GB</li>
				<li>D.D: 45GB</li>
		</div>
	</div>
	<div class="Grid u-marginTop-none u-align-center">
		<div class="ButtonList u-align-center u-marginTop-none">
			<ul>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="/descarga/WoW.rar" data-analytics="wow7-beta-sign-up" data-analytics-placement="Bottom">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text"><p style="font-size: 17px;">Cliente FULL</p></div>
					</a>
				</li>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="/descarga/WoW.rar"  data-analytics="wow7-buy" data-analytics-placement="Bottom Button">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text"><p style="font-size: 18px;">Parche NECESARIO</p></div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>







<div class="Section Section--obeliskFooter Section--bigDividerTwo" id="bottom">
	<div class="tempClass-obeliskFooterBuffer">
		<div class="Grid u-align-center">
			<div class="ContentButtonList">
				<div class="ContentButtonList-item">
					<a class="ContentButtonList-link FrameSmaller" href="http://www.warcraft.com/" data-analytics="wow7-action" data-analytics-placement="Starter Edition - Bottom">
						<div class="FrameSmaller-overlay"></div>
						<div class="FrameSmaller-side FrameSmaller-side--top"></div>
						<div class="FrameSmaller-side FrameSmaller-side--right"></div>
						<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
						<div class="FrameSmaller-side FrameSmaller-side--left"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
						<span class="ContentButtonList-content ContentButtonList-content--beginnerTrial">
						<span class="ContentButtonList-subheader">Apoya al Servidor</span>
						<span class="ContentButtonList-header">DONACIONES</span>
						</span>
					</a>
				</div>
				<div class="ContentButtonList-item">
					<a class="ContentButtonList-link FrameSmaller" href="https://battle.net/account/download/?show=wow" data-analytics="wow7-action" data-analytics-placement="Veteran Trial - Bottom">
						<div class="FrameSmaller-overlay"></div>
						<div class="FrameSmaller-side FrameSmaller-side--top"></div>
						<div class="FrameSmaller-side FrameSmaller-side--right"></div>
						<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
						<div class="FrameSmaller-side FrameSmaller-side--left"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
						<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
						<span class="ContentButtonList-content ContentButtonList-content--veteranTrial">
						<span class="ContentButtonList-subheader">Comunidad Stormmane</span>
						<span class="ContentButtonList-header">FOROS</span>
						</span>
					</a>
				</div>
			</div>
		</div>
		<div class="Grid u-align-center u-marginTop-xLarge">
			<div class="Social">
				<p class="Social-intro">Visita nuestro Facebook y siguenos por Twitter!</p>
				<ul class="Social-items">
					<li class="Social-item">
						<a class="Social-link Social-link--facebook" href="https://www.facebook.com/stormmane/" data-slug="facebook" data-analytics="wow7-sns" data-analytics-placement="facebook - Bottom">
							<span class="Social-label">Facebook</span>
						</a>
					</li>
					<li class="Social-item">
						<a class="Social-link Social-link--twitter" href="https://twitter.com/stormmaneserver" target="_blank" data-slug="twitter" data-analytics="wow7-sns" data-analytics-placement="twitter - Bottom">
							<span class="Social-label">Twitter</span>
						</a>
					</li>
					<li class="Social-item">
						<a class="Social-link Social-link--youtube" href="https://www.youtube.com/user/NyanNekoSan/featured?view_as=subscriber" data-slug="youtube" data-analytics="wow7-sns" data-analytics-placement="youtube - Bottom">
							<span class="Social-label">Canal</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="NavbarFooter is-region-limited" data-timestamp="1533157592033" data-hash="6e5d7e15804ada77c1c40831157b43fae8b3577e" data-region-selection="limited" data-region="eu" data-locale="es-es" >
		<div class="NavbarFooter-overlay"></div>
		<div class="NavbarFooter-legal">
			es-ES
		</div>
		<div class="NavbarFooter-logoContainer">
			<a href="https://www.blizzard.com/" class="NavbarFooter-logo">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 124 66" class="NavbarFooter-logoUpper">
					<use xlink:href="#NavbarFooter-blizzard-upper"></use>
				</svg>
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 124 66" class="NavbarFooter-logoLower">
					<use xlink:href="#NavbarFooter-blizzard-lower"></use>
				</svg>
			</a>
		</div>
		<div class="NavbarFooter-links NavbarFooter-mainLinks">
			<div class="NavbarFooter-linksLeft">
				<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://discord.gg/SygPvbw" class="NavbarFooter-anchor" data-id="careers" data-analytics="global-nav" data-analytics-placement="Footer - Careers">DISCORD</a></div>
				<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://chat.whatsapp.com/HXoghR2Wjd1L2qZ6IhzThV" class="NavbarFooter-anchor" data-id="about" data-analytics="global-nav" data-analytics-placement="Footer - About">WHATSAPP</a></div>
				<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://" class="NavbarFooter-anchor" data-id="support" data-analytics="global-nav" data-analytics-placement="Footer - Support">FOROS</a></div>
			</div>
			<div class="NavbarFooter-linksRight">
				<!--<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="http://" class="NavbarFooter-anchor" data-id="contact" data-analytics="global-nav">Términos de Condición de uso</a></div>
				<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://" class="NavbarFooter-anchor" data-id="press" data-analytics="global-nav" data-analytics-placement="Footer - Press">Política de Privacidad</a></div>-->
				<div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://" class="NavbarFooter-anchor" data-id="api" data-analytics="global-nav" data-analytics-placement="Footer - Battle.net API">Política de reembolso</a></div>
			</div>
		</div>
		<div class="NavbarFooter-copyright">©2017-2018. STORMMANE • Servidor Privado</div>
		<div class="NavbarFooter-trademark">Todas las marcas registradas referenciadas son propiedad de sus respectivos dueños.</div>
		<div class="NavbarFooter-links NavbarFooter-subLinks">
			<div class="NavbarFooter-link NavbarFooter-subLink"><a href="https://" class="NavbarFooter-anchor" data-id="privacy" data-analytics="global-nav" data-analytics-placement="Footer - Privacy">Privacidad</a></div>
			<div class="NavbarFooter-link NavbarFooter-subLink"><a href="https://" class="NavbarFooter-anchor" data-id="terms" data-analytics="global-nav" data-analytics-placement="Footer - Terms">Términos</a></div>
		</div>
		<br><br><br>
	</div>
</div>

<div class="Section Section--obeliskFooter Section--bigDividerTwo">
</div>


<div id="modal" class="modal hide">
	<div class="modal-content Frame Frame--inactive">
		<div class="Frame-overlay"></div>
		<div class="Frame-side Frame-side--top"></div>
		<div class="Frame-side Frame-side--right"></div>
		<div class="Frame-side Frame-side--bottom"></div>
		<div class="Frame-side Frame-side--left"></div>
		<div class="Frame-corner Frame-corner--topLeft"></div>
		<div class="Frame-corner Frame-corner--topRight"></div>
		<div class="Frame-corner Frame-corner--bottomRight"></div>
		<div class="Frame-corner Frame-corner--bottomLeft"></div>
		<div class="modal__media"></div>
		<div class="modal-controls">
			<div class="prev arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
			<div class="next arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
		</div>
		<span class="media-btn close">×</span>
	</div>
</div>

<div id="login" class="modal hide in" aria-hidden="false" style="display: none;">
	<div class="modal-content Frame Frame--inactive video" style="max-width: 50%;">
		<div class=""></div>
		<div class="Frame-side Frame-side--top"></div>
		<div class="Frame-side Frame-side--right"></div>
		<div class="Frame-side Frame-side--bottom"></div>
		<div class="Frame-side Frame-side--left"></div>
		<div class="Frame-corner Frame-corner--topLeft"></div>
		<div class="Frame-corner Frame-corner--topRight"></div>
		<div class="Frame-corner Frame-corner--bottomRight"></div>
		<div class="Frame-corner Frame-corner--bottomLeft"></div>
		<div>
			<form id="formIniciarSesion">
				<h1>Iniciar Sesión</h1>
				<div class="inset">
					<p>
						<div class="w3-light-grey w3-round cargando" style="display: none;">
							<div id="loaderCargar1" class="w3-container w3-blue w3-round" style="height:4px;width:1%;padding: 0.01em 1px;font-size: 10px;color: #0dc61d!important;"></div>
						</div>
						<label for="usuarioLogin">Correo o Usuario</label>
						<input type="text" name="usuarioLogin" id="usuarioLogin" required="required">
						<label for="passwordLogin">Contraseña</label>
						<input type="password" name="passwordLogin" id="passwordLogin" required="required">
					</p>
				</div>
				<p class="p-container">
					<a href="#" title="recuperar Contraseña" onclick="$('#formIniciarSesion').css('display','none'),$('#recuperarPass').css('display','block'); xModal = 3;"><span>Recuperar Contraseña</span></a>
					<a id="btnRegistrar" class="" title="Registrarme" href="#" ><span>&nbsp;&nbsp;&nbsp;Registrarme</span></a>
					<input type="submit" name="enviar" id="enviar" value="Iniciar Sesión">
				</p>
			</form>

			<form id="recuperarPass" style="display: none;">
				<h1>Iniciar Sesión</h1>
				<div class="inset">
					<div  class="w3-light-grey w3-round cargando" style="display: none;">
						<div id="loaderCargar3" class="w3-container w3-blue w3-round" style="height:4px;width:1%;padding: 0.01em 1px;font-size: 10px;color: #0dc61d!important;"></div>
					</div>
					<br>
					<p>
						<label for="usuarioRecuperar">Escriba su Correo para recuperacíon</label>
						<input type="email" name="usuarioRecuperar" id="usuarioRecuperar" required="required">
					</p>
				</div>
				<p class="p-container">
					<a href="#" onclick="$('#recuperarPass').css('display','none'),$('#formIniciarSesion').css('display','block')"><span>Volver</span></a>
					<input type="submit" name="enviarRecuperar" id="enviarRecuperar" value="Recuperar Contraseña">
				</p>
			</form>

		</div>
		<div class="modal-controls">
			<div class="prev arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
			<div class="next arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
		</div>
		<span class="media-btn close"><a href="#" onclick="$('#login').modal('toggle')">×</a></span>
	</div>
</div>

<div id="registrar" class="modal hide in" aria-hidden="false" style="display: none;">
	<div class="modal-content Frame Frame--inactive video"  style="max-width: 50%;">
		<div class=""></div>
		<div class="Frame-side Frame-side--top"></div>
		<div class="Frame-side Frame-side--right"></div>
		<div class="Frame-side Frame-side--bottom"></div>
		<div class="Frame-side Frame-side--left"></div>
		<div class="Frame-corner Frame-corner--topLeft"></div>
		<div class="Frame-corner Frame-corner--topRight"></div>
		<div class="Frame-corner Frame-corner--bottomRight"></div>
		<div class="Frame-corner Frame-corner--bottomLeft"></div>
		<div>
			<form id="formRegistrar">
				<h1>Registtrar</h1>
				<div class="inset">
					<p>
						<div class="w3-light-grey w3-round cargando" style="display: none;">
							<div id="loaderCargar2" class="w3-container w3-blue w3-round" style="height:4px;width:1%;padding: 0.01em 1px;font-size: 10px;color: #0dc61d!important;"></div>
						</div>
						<label for="correoRegistro">Correo electronico</label>
						<input type="email" onblur="return validarCorreo();" name="correoRegistro" id="correoRegistro">
						<label for="usuarioRegistro">Usuario</label>
						<input type="text" onblur="return validarUsuario();" name="usuarioRegistro" id="usuarioRegistro">
						<label for="passwordRegistro1">Contraseña</label>
						<input type="password" name="passwordRegistro1" id="passwordRegistro1">
						<label for="passwordRegistro2">Vuelva a escribir su contraseña</label>
						<input type="password" name="passwordRegistro2" id="passwordRegistro2">
					</p>
				</div>
				<p class="p-container">
					<input type="submit" name="enviarRegistro" id="enviarRegistro" value="Registrar">
				</p>
			</form>
		</div>
		<div class="modal-controls">
			<div class="prev arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
			<div class="next arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
		</div>
		<span class="media-btn close"><a href="#" onclick="$('#registrar').modal('toggle')">×</a></span>
	</div>
</div>

<div id="estado" class="modal hide in" aria-hidden="false" style="display: none;">
	<div class="modal-content Frame Frame--inactive video"  style="max-width: 50%;">
		<div class=""></div>
		<div class="Frame-side Frame-side--top"></div>
		<div class="Frame-side Frame-side--right"></div>
		<div class="Frame-side Frame-side--bottom"></div>
		<div class="Frame-side Frame-side--left"></div>
		<div class="Frame-corner Frame-corner--topLeft"></div>
		<div class="Frame-corner Frame-corner--topRight"></div>
		<div class="Frame-corner Frame-corner--bottomRight"></div>
		<div class="Frame-corner Frame-corner--bottomLeft"></div>
		<div>
			<form style="font-size: 15px;">
				<h2>Servidor <?=$this->config->item("nombreServidor");?></h2>

				<div class="inset">
					<p align="left"><i class="fas fa-server"></i> Estado del Servidor</p>
					<h6 align="center"><i class="fas fa-gamepad"></i> Realmlist : <?=$this->config->item("realmlist")?></h6>
				</div>

				<div class="inset">
					<p align="left"><i class="fas fa-globe"></i> Online <?=$online?></p>
					<span class="uk-label">
    					<span id="alizOnline"  uk-icon="icon: users" class="uk-icon">
						<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
							<circle fill="none" stroke="#000" stroke-width="1.1" cx="7.7" cy="8.6" r="3.5"></circle>
							<path fill="none" stroke="#000" stroke-width="1.1" d="M1,18.1 C1.7,14.6 4.4,12.1 7.6,12.1 C10.9,12.1 13.7,14.8 14.3,18.3"></path>
							<path fill="none" stroke="#000" stroke-width="1.1" d="M11.4,4 C12.8,2.4 15.4,2.8 16.3,4.7 C17.2,6.6 15.7,8.9 13.6,8.9 C16.5,8.9 18.8,11.3 19.2,14.1"></path>
						</svg>
					</span><?=$alizOnline;?> Alianza</span>
					<span class="uk-label uk-label-success">
						<span id="hodaOnline" uk-icon="icon: users" class="uk-icon">
							<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
								<circle fill="none" stroke="#000" stroke-width="1.1" cx="7.7" cy="8.6" r="3.5"></circle>
									<path fill="none" stroke="#000" stroke-width="1.1" d="M1,18.1 C1.7,14.6 4.4,12.1 7.6,12.1 C10.9,12.1 13.7,14.8 14.3,18.3"></path>
									<path fill="none" stroke="#000" stroke-width="1.1" d="M11.4,4 C12.8,2.4 15.4,2.8 16.3,4.7 C17.2,6.6 15.7,8.9 13.6,8.9 C16.5,8.9 18.8,11.3 19.2,14.1"></path>
							</svg>
					</span><?=$pandaNuestral?> Neutral Panda</span>
					<span class="uk-label uk-label-danger">
						<span id="hodaOnline" uk-icon="icon: users" class="uk-icon">
							<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
								<circle fill="none" stroke="#000" stroke-width="1.1" cx="7.7" cy="8.6" r="3.5"></circle>
									<path fill="none" stroke="#000" stroke-width="1.1" d="M1,18.1 C1.7,14.6 4.4,12.1 7.6,12.1 C10.9,12.1 13.7,14.8 14.3,18.3"></path>
									<path fill="none" stroke="#000" stroke-width="1.1" d="M11.4,4 C12.8,2.4 15.4,2.8 16.3,4.7 C17.2,6.6 15.7,8.9 13.6,8.9 C16.5,8.9 18.8,11.3 19.2,14.1"></path>
							</svg>
					</span><?=$hordaOnline?> Horda</span>
				</div>
				<div class="inset">
					<h6 align="left"><i class="fab fa-discord"></i> Discord</h6>
					<p align="center"><a href="<?=$this->config->item("LinkIvinteDiscord")?> target="_blank""><?=$discord["nombre"]?></a></p>
					<a href="<?=$this->config->item("LinkIvinteDiscord")?>" target="_blank" title="<?=$discord["nombre"]?>"><img src="<?=$discord["imagen"];?>" class="uk-border-circle" width="70" height="70"></a>
					<br>
					<span class="uk-label uk-label-warning">
						<a href="<?=$this->config->item("LinkIvinteDiscord")?>" target="_blank" title="<?=$discord["nombre"]?>"><?=$discord["nombreCanal"]?> </a>
					</span>
					&nbsp;&nbsp;
					<span class="uk-label uk-label-success">
						<a href="<?=$this->config->item("LinkIvinteDiscord")?>" target="_blank" title="<?=$discord["nombre"]?>"> Online <?=$discord["online"]?> </a>
					</span>

				</div>
				<div class="inset">
					<a href="<?=$this->config->item("whatsapp")?>" target="_blank" title="whatsapp"><img src="/assets/custom/wpp.png" class="uk-border-circle" width="120"></a>
				</div>
			</form>
		</div>
		<div class="modal-controls">
			<div class="prev arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
			<div class="next arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
		</div>
		<span class="media-btn close"><a href="#" onclick="$('#estado').modal('toggle')">×</a></span>
	</div>
</div>

<script src="<?=base_url("assets/notify.js")?>"></script>
<script>

	var url = '<?=base_url("/Wow/")?>';
	var uV = true;
	var cV = true;
	var xModal = 0;

	 function iniciarSession(){
		 $("#formIniciarSesion").css("display","block");
		<?php
			if(isset($this->session->idUsuario)){
				echo 'window.location.href = "'.base_url("/panel").'"';
			}else{
				echo '$("#login").modal("show");$("#recuperarPass").css("display","none");xModal = 1;';
			}
		 ?>
	 }

	 $("#btnRegistrar").click(function () {
		 $("#login").modal("toggle");
		 $("#formIniciarSesion").css("display","none");
		 $("#registrar").modal("show");
		 $("#recuperarPass").css("display","none");
		 xModal = 2;
	 });



	 $("#formIniciarSesion").on("submit",function (form){
		 form.preventDefault();
		 $(".cargando").css("display","block");
		 move();
		 $.ajax({
			 url: url+"iniciarSesion",
			 dataType : 'JSON',
			 type : 'POST',
			 data : {
				 usuario : $("#usuarioLogin").val(),
				 password : $("#passwordLogin").val(),
			 },
			 success:function (data) {
				 $(".cargando").css("display","none");
				 if(!data){
					 $.notify("Usuario Y/o Contraseña invalidad","error")
				 }else{
					 $.notify("Correcto, cargando datos...","success");
					 window.location.href = '<?=base_url('/panel/')?>'
				 }
			 }
		 });
 	});


	$("#formRegistrar").on("submit",function (form){
		form.preventDefault();
		$(".cargando").css("display","block");
		if($("#passwordRegistro1").val() != $("#passwordRegistro2").val()){
			$.notify("Las contraseñas deben coincidir","error")
			return;
		}
		if(uV && cV){
			registrar();
		}else{
			$.notify("¡Ojo! Correo o Usuario ya en uso","error");
		}

	});

	function registrar() {
		move();
		$.ajax({
			url: url+"registrarUsuario",
			dataType : 'JSON',
			type : 'POST',
			data : {
				email : $("#correoRegistro").val(),
				usuario : $("#usuarioRegistro").val(),
				password : $("#passwordRegistro1").val()
			},
			success:function (data) {
				if(!data){
					$(".cargando").css("display","none");
					$.notify("No se pudo registrar Usuario","error")
				}else{
					$.notify("Correcto, cargando datos...","success");
					setTimeout(function () {
						$.notify("Datos cargado con exito, Debes iniciar sesion","info");
					}, 2000); //will call the function after 2 secs.

				}
			}
		});
	}

	function validarCorreo() {
		$.ajax({
			url: url+"validarExits?id=1", dataType : 'JSON', type : 'POST', data : {data : $("#correoRegistro").val()},
			success : function (data) {
				if(data){
					$.notify("Correo ya existe","error");
					uV = true;
					cV = true;
				}
			}
		});
	}
	function validarUsuario() {
		$.ajax({
			url: url+"validarExits?id=2", dataType : 'JSON', type : 'POST', data : {data : $("#usuarioRegistro").val()},
			success : function (data) {
				if(data){
					$.notify("Usuario ya existe","error");
					uV = false;
				}else{
					 uV = true;
					 cV = true;
				}
			}
		});
	}

	setTimeout(function () {
		$("#media").css("display","none");
	}, 2000); //will call the function after 2 secs.

	$("#recuperarPass").on("submit",function (form) {
		form.preventDefault();
		$("#enviarRecuperar").addClass("not-active");
		$(".cargando").css("display","block");
		move();
		$.ajax({
			url: url+"recuperacion", dataType : 'JSON', type : 'POST', data : {email : $("#usuarioRecuperar").val()},
			success : function (data) {
				$(".cargando").css("display","none");
				$("#enviarRecuperar").removeClass("not-active");
				if(data == true || data == 1){
					$.notify("Por favor revise su recorreo electronico","success");
				}else{
					$.notify("Datos suministrados no son correctos");
				}
			}
		});
	});


	function move() {

		if(xModal == 1 || xModal == '1'){
			var elem = document.getElementById("loaderCargar1");
		}else if(xModal == 2 || xModal == '2'){
			var elem = document.getElementById("loaderCargar2");
		}else{
			var elem = document.getElementById("loaderCargar3");
		}

		var width = 1;
		var id = setInterval(frame, 15);
		function frame() {
			if (width >= 100) {
				clearInterval(id);
			} else {
				width++;
				elem.style.width = width + '%';
				$("#loaderCargar").html(width+"%")
			}
		}
	}

	$("#login").on('hidden.bs.modal', function () {
		$(".cargando").css("display","none");
	});
	$("#registrar").on('hidden.bs.modal', function () {
		$(".cargando").css("display","none");
	});

	/**/



	$(document).ready(function () {

		setTimeout(function () {
			$("a.Navigation-link--home").click(function (e) {
				"use strict";
				window.location.href = "#home";
				e.preventDefault();
			});

			$("a.Navigation-link--class").click(function (e) {
				"use strict";
				window.location.href = "#class";
				e.preventDefault();
			});

			$("a.Navigation-link--artifacts").click(function (e) {
				"use strict";
				window.location.href = "#artifacts";
				e.preventDefault();
			});

			$("a.Navigation-link--media").css("display","none");

			$("a.Navigation-link--world").click(function (e) {
				"use strict";
				window.location.href = "#world";
				e.preventDefault();
			});

			$("a.Navigation-link--characters").click(function (e) {
				"use strict";
				window.location.href = "#characters";
				e.preventDefault();
			});

			$("a.Navigation-link--features").click(function (e) {
				"use strict";
				window.location.href = "#features";
				e.preventDefault();
			});

		},500);
	});




</script>


<!-- Font tracking script. -->
<script type="text/javascript">
	//<![CDATA[
	new Image().src = "//fast.fonts.net/t/1.css?apiType=css&projectid=23f45f39-cbf3-41cf-912f-e015fde06357";
	//]]>
</script>
<script type="text/javascript" src="//bnetcmsus-a.akamaihd.net/cms/template_resource/wow/js/wow.min.1.js?v=58-127"></script>

<script type="text/javascript" src="/assets/wow/static/js/navbar.js?v=58-127"></script>
<script type="text/javascript" src="/assets/wow/static/js/seven-oh/dist/seven-oh.min.js?v=127"></script>




</body>
</html>
