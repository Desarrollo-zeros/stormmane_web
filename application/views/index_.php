<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" class="es-es">
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<meta http-equiv="imagetoolbar" content="false" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Legion</title>
	<link rel="icon" href="/assets/wow/static//images/icons/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/assets/wow/static/images//icons/favicon.ico" type="image/x-icon" />
	<link rel="search" type="application/opensearchdescription+xml" href="http://eu.battle.net/es-es/data/opensearch" title="Búsqueda eb Battle.net" />
	<script type="text/javascript" src="/assets/wow/static//local-common/js/third-party.js?v=58"></script>
	<script type="text/javascript" src="/assets/wow/static//local-common/js/common-game-site.min.js?v=58"></script>
	<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static//wow.min.1.css?v=58-127" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static///wow.wow.min.29.css?v=58-127" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="Legion" />
	<script type="text/javascript">
		//<![CDATA[
		var Core = Core || {},
			Login = Login || {};
		Core.staticUrl			= '/assets/wow/static/';
		Core.sharedStaticUrl 	= '/assets/wow/static/local-common';
		Core.baseUrl			= '/assets/wow/static/es';
		Core.projectUrl     	= '/wow';
		Core.cdnUrl         	= '<?=base_url()?>';
		Core.supportUrl			= '<?=base_url()?>';
		Core.secureSupportUrl 	= '<?=base_url()?>';
		Core.project			= 'wow';
		Core.locale				= 'es-es';
		Core.language			= 'es';
		Core.region				= 'eu';
		Core.shortDateFormat 	= 'dd/MM/yyyy';
		Core.dateTimeFormat		= 'dd/MM/yyyy HH:mm';
		Core.loggedIn			= false;
		Core.userAgent			= 'web';
		Login.embeddedUrl 		= '<?=base_url()?>';
		var Flash = Flash || {};
		Flash.videoPlayer		= '<?=base_url()?>//assets/wow/static//images/video-player.swf';
		Flash.videoBase			= '<?=base_url()?>//assets/wow/static//images/';
		Flash.ratingImage		= '<?=base_url()?>//assets/wow/static//images/es-es.jpg';
		Flash.expressInstall 	= '<?=base_url()?>//assets/wow/static//images/expressInstall.swf';
		var _gaq = _gaq || [];

		_gaq.push(['_setAccount', 'UA-544112-16']);

		_gaq.push(['_setDomainName', '.battle.net']);

		_gaq.push(['_setAllowLinker', true]);




		_gaq.push(['_trackPageview']);
		//]]>
	</script>
	<meta property="fb:app_id" content="175314325675" />
	<meta property="og:site_name" content="World of Warcraft" />
	<meta property="og:locale" content="es_ES" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="/assets/wow/static//images/og-wow.png" />
	<meta property="og:image" content="/assets/wow/static//images/og-blizzard.png" />
	<meta property="og:title" content="World of Warcraft" />
	<meta property="og:description" content="Legion - Legion - World of Warcraft" />
</head>
<body class="es-es page full-page-view">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<link rel="image_src" href="/assets/wow/static//images/og-wow.png" />
<meta property="og:site_name" content="World of Warcraft®" />
<meta property="og:title" content="Legion - Legion - World of Warcraft" />
<meta property="og:image" content="/assets/wow/static//images/og-wow.png" />
<meta property="og:url" content="http://eu.battle.net/wow/es/legion/" />
<meta property="og:type" content="website" />
<meta property="fb:admins" content="100001571535053" />
<script>
	//<![CDATA[
	var dataLayer = [{
		"authenticated": "0"            }];
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!="dataLayer"?"&amp;l="+l:"";j.async=true;j.src=
		"//www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);})
	(window,document,"script","dataLayer","GTM-M39SFZ");
	//]]>
</script>
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/local-common/css/common.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/navbar.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/legal/ratings.css?v=58-127" />
<link rel="stylesheet" type="text/css" media="all" href="/assets/wow/static/css/seven-oh/seven-oh.css?v=127" />
<div class="Navigation">
	<div class="Navigation-full">
		<div class="Navigation-fullWrap">
			<div class="VertAlign">
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--collapse">
						<ul class="Navigation-list Navigation-list--gameSite">
							<li class="Navigation-item">
								<a class="Navigation-link" href="http://eu.battle.net/wow/es" data-analytics="wow7-action" data-analytics-placement="Game Site">
									<span class="Navigation-label">Ir a World of Warcraft</span>
									<span class="Navigation-icon Navigation-icon--gameSite"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--middle">
						<ul class="Navigation-list Navigation-list--buy">
							<li class="Navigation-item Navigation-item--hideInFull">
								<a class="Navigation-link" href="https://www.battle.net/download/getInstallerForGame?gameProgram=WORLD_OF_WARCRAFT" data-analytics="wow7-beta-sign-up" data-analytics-placement="Nav">
									<span class="Navigation-label">Descargar juego</span>
									<span class="Navigation-icon Navigation-icon--buy"></span>
								</a>
							</li>
							<li class="Navigation-item Navigation-item--hideInFull">
								<a class="Navigation-link" href="https://eu.shop.battle.net/es-es/family/world-of-warcraft" data-analytics="wow7-buy" data-analytics-placement="Nav Button">
									<span class="Navigation-label">Jugar</span>
									<span class="Navigation-icon Navigation-icon--buy"></span>
								</a>
							</li>
						</ul>
						<ul class="Navigation-list Navigation-list--sections">
							<li class="Navigation-item Navigation-item--buyBox Navigation-item--hideInCompact">
								<a class="Navigation-link" href="https://eu.shop.battle.net/es-es/family/world-of-warcraft" data-analytics="wow7-buy" data-analytics-placement="Pre-Purchase - Nav">
									<div class="Navigation-label">Descargar</div>
									<div class="Navigation-icon Navigation-icon--buyBox"></div>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="VertAlign-section">
					<div class="VertAlign-block VertAlign-block--collapse">
						<ul class="Navigation-list Navigation-list--social">
							<li class="Navigation-item">
								<a class="Navigation-link" href="http://www.facebook.com/WorldofWarcraft.es" data-slug="facebook" data-analytics="wow7-sns" data-analytics-placement="facebook - Nav">
									<div class="Navigation-label">World of Warcraft en Facebook</div>
									<div class="Navigation-icon Navigation-icon--facebook"></div>
								</a>
							</li>
							<li class="Navigation-item">
								<a class="Navigation-link" href="http://twitter.com/Warcraft_ES" data-slug="twitter" data-analytics="wow7-sns" data-analytics-placement="twitter - Nav">
									<div class="Navigation-label">World of Warcraft en Twitter</div>
									<div class="Navigation-icon Navigation-icon--twitter"></div>
								</a>
							</li>
							<li class="Navigation-item">
								<a class="Navigation-link" href="http://www.youtube.com/user/WorldofWarcraftES" data-slug="youtube" data-analytics="wow7-sns" data-analytics-placement="youtube - Nav">
									<div class="Navigation-label">Canal de World of Warcraft</div>
									<div class="Navigation-icon Navigation-icon--youtube"></div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Navigation-compact">
		<div class="Navigation-compactToggle"></div>
		<div class="Navigation-compactWrap">
			<div class="Navigation-compactLogoWrap">
				<img class="Navigation-compactLogo" src="/assets/wow/static/images//seven-oh/logo.png" alt="World of Warcraft" />
			</div>
		</div>
		<div class="Navigation-compactShield"></div>
	</div>
</div>
<div class="Section Section--keyArtHeader VideoBackground VideoBackground--wide VideoBackground--responsiveBackdrop" id="home" data-nav="Inicio">
	<div class="VideoBackground-container">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/header-illidan-still.jpg" data-mp4="/assets/wow/static/images//header-illidan-still-mp4.jpg" />
	</div>
	<div class="Grid u-align-center">
		<h1 class="Logo">
			<img class="ScaledImage ScaledImage--logo" src="/assets/wow/static/images//seven-oh/logo.png" alt="World of Warcraft" />
		</h1>
		<div class="ButtonList ButtonList--keyArtHeader">
			<ul>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="https://www.battle.net/download/getInstallerForGame?gameProgram=WORLD_OF_WARCRAFT" data-analytics="wow7-beta-sign-up" data-analytics-placement="Top">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text">Descargar juego</div>
					</a>
					<a class="ButtonList-itemLabel ButtonList-itemLabel--link" href="http://www.warcraft.com/"data-analytics="wow7-action" data-analytics-placement="Starter Edition - Top">Ver Online</a>
				</li>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="https://eu.shop.battle.net/es-es/family/world-of-warcraft" data-analytics="wow7-buy" data-analytics-placement="Top Button">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text">Jugar</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="InlineList">
			<div class="VideoThumbnail VideoThumbnail--large VideoThumbnail--keyArtHeaderCenter InlineList-item">
				<!-- here -->
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=9dtElNTvlN4" data-youtube-id="9dtElNTvlN4"  data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images/RHJhZW5vblVwZ3JhZGUy.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images/RHJhZW5vblVwZ3JhZGUy.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="assets/wow/static/images/RHJhZW5vblVwZ3JhZGU.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Tráiler cinemático</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
			<div class="VideoThumbnail VideoThumbnail--keyArtHeaderLeft InlineList-item">
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=eTz5MO8XFyA" data-youtube-id="eTz5MO8XFyA"  data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images/teaser.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images/teaser.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="/assets/wow/static/images/teaser-thumb.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Avance</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
			<div class="VideoThumbnail VideoThumbnail--keyArtHeaderRight InlineList-item">
				<a class="VideoThumbnail-link " href="//www.youtube.com/watch?v=jgkAVRwPV9I" data-youtube-id="jgkAVRwPV9I"  data-analytics="wow7-video-youtube" data-analytics-placement="Top">
					<div class="VideoThumbnail-videoWrap">
						<video class="VideoThumbnail-video" autoplay="autoplay" loop="loop">
							<source src="/assets/wow/static/images//announcement.mp4" type="video/mp4"></source>
							<source src="/assets/wow/static/images//announcement.webm" type="video/webm"></source>
						</video>
						<img class="VideoThumbnail-fallback" src="/assets/wow/static/images/announcement-thumb.jpg" alt="" />
						<div class="VideoThumbnail-playButton"></div>
					</div>
					<div class="VideoThumbnail-label">Vídeo de presentación</div>
					<div class="VideoThumbnail-underGlow"></div>
					<div class="VideoThumbnail-overGlow"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="Grid u-align-center u-marginTop-xLarge u-zIndex-3">
		<div class="Body Body--keyArtHeader">
			<h1 class="Header--wideLetterSpacing">30.08.16<br />LOS REINOS ARDERÁN</h1>
			<p>La Tumba de Sargeras se ha reabierto, y ahora los demonios de la Legión Ardiente entran a raudales en nuestro mundo. Están concentrando todo su terrorífico poder para invocar al titán oscuro en Azeroth… y ya han encontrado la clave para que regrese.</p>
			<p>Ahora que la Alianza y la Horda han sido vencidas, solo tú podrás empuñar los artefactos más legendarios de <em>Warcraft</em>, explorar las ancestrales Islas Abruptas en busca de reliquias de los titanes y enfrentarte a la Legión antes de que la última esperanza de Azeroth se desvanezca.</p>
			<p>Prepárate, adalid. La extinción se acerca.</p>
			<p/>
		</div>
	</div>
</div>
<div class="Section Section--bigDividerOne VideoBackground VideoBackground--tall VideoBackground--wide" id="class" data-nav="Clase héroe">
	<div class="VideoBackground-container u-tablet-hidden">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/demon-hunters-still.jpg" />
		<div class="VideoBackground-classTransition"></div>
	</div>
	<div class="Grid Grid--narrowGrid u-align-center">
		<h2>Clase héroe</h2>
		<h1>Acecha a tus presas como cazador de demonios</h1>
		<p>Domina a tus adversarios como cazador de demonios, una clase que tiene sus orígenes en los elfos que fueron desterrados por atreverse a tomar los terribles poderes de la Legión. Los cazadores de demonios poseen una movilidad superior y un nivel sobrenatural de percepción, y pueden acceder a poderes prohibidos en momentos de necesidad para transformarse en seres aterradores. La especialización Caos te permitirá aniquilar a todo aquel que se cruce en tu camino con abrasadores ataques demoníacos, mientras que con la especialización Venganza podrás enfrentarte a los demonios más poderosos y someterte a un castigo infinito para que sus ataques exalten tu odio.</p>
	</div>
	<div class="PaginatorByIcon PaginatorByIcon--simple PaginatorByIcon--class u-desktop-hidden">
		<div class="PaginatorByIcon-iconsWrap">
			<ul class="PaginatorByIcon-icons"></ul>
		</div>
		<div class="PaginatorByIcon-content">
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/demon-hunter-female.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/demon-hunter-male.jpg" alt="" />
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
	<div class="MediaList Grid Grid--narrowGrid u-marginTop-xLarge">
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/spectral-sight-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Visión espectral</h3>
				<p class="Paragraph--small">
				<p>La aparente ceguera de los cazadores de demonios camufla sus verdaderos poderes de percepción. Hacen uso de su visión aumentada para detectar enemigos, aun si estos están ocultos detrás de obstáculos.</p>
				</p>
			</div>
		</div>
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/metamorphosis-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Metamorfosis</h3>
				<p class="Paragraph--small">Los cazadores de demonios se transforman en criaturas infernales para potenciar su especialización: los Illidari centrados en infligir daño obtienen velocidad y daño sobrenaturales, lo que les permite dar muerte rápida a sus presas; los que optan por un papel defensivo son prácticamente inmortales en su forma demoníaca.</p>
			</div>
		</div>
		<div class="MediaList-item Grid-cell Grid-cell--1of3 Large-cell--1of1">
			<div class="MediaList-side">
				<img class="ScaledImage ScaledImage--classFeature" src="/assets/wow/static/images/mobility-icon.png" alt="" />
			</div>
			<div class="MediaList-main">
				<h3>Movilidad sin par</h3>
				<p class="Paragraph--small">
					<div dir="ltr" id="imcontent">
				<p><span dir="ltr">Los cazadores de demonios pueden realizar saltos dobles, irrumpir en los combates y huir de ellos, e incluso desplegar sus monstruosas alas para atacar y sorprender a los enemigos desde el cielo.</span></p>
			</div>
			</p>
		</div>
	</div>
</div>
<div class="Grid u-marginTop-xLarge">
	<div class="ThumbnailList">
		<ul class="ThumbnailList-items">
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-nelf-female-tank.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-nelf-female-tank.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-nelf-female-tank-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Venganza (Tanque)</span>
				</a>
			</li>
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-base.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-base.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-base-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Cazadores de demonios</span>
				</a>
			</li>
			<li class="ThumbnailList-item ThumbnailList-item--1of4">
				<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/class-belf-male-dps.jpg" data-analytics="wow7-image" data-analytics-placement="Class" data-analytics-asset="/assets/wow/static/images/class-belf-male-dps.jpg">
					<div class="FrameSmaller-overlay"></div>
					<div class="FrameSmaller-side FrameSmaller-side--top"></div>
					<div class="FrameSmaller-side FrameSmaller-side--right"></div>
					<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
					<div class="FrameSmaller-side FrameSmaller-side--left"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
					<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
					<img class="ThumbnailList-image" src="/assets/wow/static/images/class-belf-male-dps-thumb.jpg" alt="" />
					<span class="ThumbnailList-label">Caos (DPS)</span>
				</a>
			</li>
		</ul>
	</div>
</div>
<div xmlns="http://www.w3.org/1999/xhtml" class="Grid u-align-center">
	<p>
		<a class="Button" href="/wow/game/class/demon-hunter" data-analytics="wow7-action" data-analytics-placement="Class Page">
		</a>
	</p>
</div>
<div class="Section Section--noPaddingBottom Section--bigDividerTwo" id="artifacts" data-nav="Artefactos">
	<div class="Grid u-align-center">
		<h2>Clase héroe</h2>
		<h1>Blande armas de un poder inimaginable</h1>
		<p>Solo los veteranos más curtidos de Azeroth poseen la fortaleza necesaria para blandir artefactos legendarios contra la Legión Ardiente. Tu arma mítica aumentará de nivel contigo, y tus elecciones modificarán sus facultades y su aspecto, su sonido y la sensación que transmite en el combate. Personaliza tu artefacto y conviértelo en el instrumento perfecto para la batalla; con él, guiarás a tu facción en las situaciones más desesperadas.</p>
	</div>
	<div class="PaginatorByIcon">
		<div class="PaginatorByIcon-iconsWrap">
			<div class="Grid">
				<ul class="PaginatorByIcon-icons"></ul>
			</div>
		</div>
		<div class="PaginatorByIcon-content VideoBackground VideoBackground--tall VideoBackground--wide">
			<div class="VideoBackground-container"></div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="ashbringer" data-mp4-url="/assets/wow/static/images/ashbringer.mp4" data-webm-url="/assets/wow/static/images/ashbringer.webm" data-background-img-url="/assets/wow/static/images/ashbringer-still.jpg" data-responsive-img-url="/assets/wow/static/images/ashbringer-compact.png" data-thumbnail-url="/assets/wow/static/images/ashbringer-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/ashbringer-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Crematoria</h3>
							<h4 class="PaginatorByIcon-subheader">Espada de dos manos de paladín</h4>
							<p class="PaginatorByIcon-summary">Dicen que esta espada, forjada por Magni Barbabronce, confiere la gloria de la Luz a su portador y le permite reducir a sus enemigos a cenizas.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-ashbringer-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-ashbringer-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-ashbringer-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="maw-of-the-damned" data-mp4-url="/assets/wow/static/images/maw.mp4" data-webm-url="/assets/wow/static/images/maw.webm" data-background-img-url="/assets/wow/static/images/maw-still.jpg" data-responsive-img-url="/assets/wow/static/images/maw-compact.png" data-thumbnail-url="/assets/wow/static/images/maw-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/maw-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Fauce de los Malditos</h3>
							<h4 class="PaginatorByIcon-subheader">Hacha de dos manos de caballero de la Muerte</h4>
							<p class="PaginatorByIcon-summary">Durante eones, el demonio Sangralix el Desgarrador se sirvió de esta enorme hacha para robar la fuerza vital de sus enemigos y restablecer la suya propia. La Fauce desangra todo lo que toca. Solo sabe… engullir.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-maw-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-maw-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-maw-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="staff-of-antonidas" data-mp4-url="/assets/wow/static/images/antonidas.mp4" data-webm-url="/assets/wow/static/images/antonidas.webm" data-background-img-url="/assets/wow/static/images/antonidas-still.jpg" data-responsive-img-url="/assets/wow/static/images/antonidas-compact.png" data-thumbnail-url="/assets/wow/static/images/antonidas-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/antonidas-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Ébano Glacial, Gran Bastón de Alodi</h3>
							<h4 class="PaginatorByIcon-subheader">Bastón de mago</h4>
							<p class="PaginatorByIcon-summary">El bastón del primer guardián de Tirisfal exuda una antinatural energía glacial, lo que permite a su portador mantener una mente fría, despierta y serena incluso en las situaciones más adversas.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-antonidas-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-antonidas-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-antonidas-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="staff-of-shaohao" data-mp4-url="/assets/wow/static/images/shaohao.mp4" data-webm-url="/assets/wow/static/images/shaohao.webm" data-background-img-url="/assets/wow/static/images/shaohao-still.jpg" data-responsive-img-url="/assets/wow/static/images/shaohao-compact.png" data-thumbnail-url="/assets/wow/static/images/shaohao-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/shaohao-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Sheilun, Bastón de la Niebla</h3>
							<h4 class="PaginatorByIcon-subheader">Bastón de monje</h4>
							<p class="PaginatorByIcon-summary">El último emperador de Pandaria usó este legendario bastón para sumir su tierra en la niebla, y este reverbera con su eterno legado de sabiduría y perseverancia.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-shaohao-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-shaohao-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-shaohao-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="artifact" data-slug="windrunner" data-mp4-url="/assets/wow/static/images/windrunner.mp4" data-webm-url="/assets/wow/static/images/windrunner.webm" data-background-img-url="/assets/wow/static/images/windrunner-still.jpg" data-responsive-img-url="/assets/wow/static/images/windrunner-compact.png" data-thumbnail-url="/assets/wow/static/images/windrunner-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/windrunner-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Thas’dorah, Legado de los Brisaveloz</h3>
							<h4 class="PaginatorByIcon-subheader">Arco de cazador</h4>
							<p class="PaginatorByIcon-summary">Cuenta la leyenda que esta reliquia de familia élfica —otrora empuñada por un infame General Forestal de Lunargenta— puede convertir a un arquero mediocre en un tirador de élite… verdaderamente inigualable.</p>
							<div class="ThumbnailList u-marginTop-medium">
								<ul class="ThumbnailList-items">
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-1.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-1.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-1-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-2.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-2.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-2-thumb.jpg" alt="" />
										</a>
									</li>
									<li class="ThumbnailList-item ThumbnailList-item--1of3">
										<a class="ThumbnailList-link FrameSmaller" href="/assets/wow/static/images/artifact-windrunner-3.jpg" data-analytics="wow7-image" data-analytics-placement="Artifact" data-analytics-asset="/assets/wow/static/images/artifact-windrunner-3.jpg">
											<div class="FrameSmaller-overlay"></div>
											<div class="FrameSmaller-side FrameSmaller-side--top"></div>
											<div class="FrameSmaller-side FrameSmaller-side--right"></div>
											<div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
											<div class="FrameSmaller-side FrameSmaller-side--left"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
											<div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
											<img class="ThumbnailList-image" src="/assets/wow/static/images/artifact-windrunner-3-thumb.jpg" alt="" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
	<div class="ButtonList u-align-center u-marginTop-none LearnMoreButtonContainer LearnMoreButtonContainer--artifacts">
		<ul>
			<li class="ButtonList-item">
				<a class="Button Button--fixedWidth" href="https://battle.net/wow/game/artifacts/" data-analytics="" data-analytics-placement="">
					<div class="Button-left"></div>
					<div class="Button-middle"></div>
					<div class="Button-right"></div>
					<div class="Button-light"></div>
					<div class="Button-text">Más información</div>
				</a>
			</li>
		</ul>
	</div>
</div>
<div class="Section Section--noPadding Section--bigDividerOne VideoBackground VideoBackground--overlay" id="world" data-nav="Escenario">
	<div class="Grid tempClass-alignTop u-align-center">
		<h2>Escenario</h2>
		<h1>Explora las Islas Abruptas</h1>
	</div>
	<div class="VideoBackground-container u-tablet-hidden">
		<img class="VideoBackground-fallback" src="/assets/wow/static/images/world-dissolve-still.jpg" />
		<div class="VideoBackground-overlay VideoBackground-overlay--dark">
			<div class="VideoBackground-overlaySize"></div>
		</div>
	</div>
	<div class="PaginatorByIcon PaginatorByIcon--simple u-desktop-hidden">
		<div class="PaginatorByIcon-iconsWrap">
			<ul class="PaginatorByIcon-icons"></ul>
		</div>
		<div class="PaginatorByIcon-content">
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/map-fullsize-esES.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-1.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-2.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-3.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-4.jpg" alt="" />
			</div>
			<div class="PaginatorByIcon-contentItem">
				<img src="/assets/wow/static/images/world-compact-5.jpg" alt="" />
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
	<div class="Grid tempClass-alignBottom">
		<div class="Grid-cell Grid-cell--3of4 Grid-push-cell--1of4 Wide-cell--1of1">
			<p>Te aguardan las Islas Abruptas, epicentro de la invasión demoníaca y tierra repleta de maravillas ancestrales, con frondosos bosques, colosales sierras y ciudades de los elfos de la noche más antiguas que la civilización humana. No obstante, aquí también acecha el peligro: sátiros, drógbar y Kvaldir malditos vagan por las Islas junto al errante ejército de la Legión. Para superar estas amenazas, controlarás una sede única para la clase de tu personaje y dirigirás a tus seguidores en la búsqueda de los Pilares de la Creación, el secreto de la salvación de Azeroth.</p>
		</div>
		<div class="Grid-cell Grid-cell--1of4 Grid-pull-cell--3of4 Wide-cell--1of1 u-tablet-hidden">
			<div class="ThumbnailList">
				<ul class="ThumbnailList-items">
					<li class="ThumbnailList-item ThumbnailList-item--noBottomPadding">
						<a class="ThumbnailList-link Frame" href="/assets/wow/static/images/map-fullsize-esES.jpg" data-analytics="wow7-image" data-analytics-placement="Map" data-analytics-asset="/assets/wow/static/images/map-fullsize-esES.jpg">
							<div class="Frame-overlay"></div>
							<div class="Frame-side Frame-side--top"></div>
							<div class="Frame-side Frame-side--right"></div>
							<div class="Frame-side Frame-side--bottom"></div>
							<div class="Frame-side Frame-side--left"></div>
							<div class="Frame-corner Frame-corner--topLeft"></div>
							<div class="Frame-corner Frame-corner--topRight"></div>
							<div class="Frame-corner Frame-corner--bottomRight"></div>
							<div class="Frame-corner Frame-corner--bottomLeft"></div>
							<img class="ThumbnailList-image" src="/assets/wow/static/images/map-thumbnail.jpg" alt="" />
							<span class="ThumbnailList-label ThumbnailList-label--forFrame">Ver mapa</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="Section Section--noPaddingBottom Section--bigDividerTwo" id="characters" data-nav="Héroes y villanos">
	<div class="Grid">
		<h2>Héroes y villanos</h2>
		<h1>Un mundo dividido</h1>
	</div>
	<div class="PaginatorByIcon">
		<div class="PaginatorByIcon-iconsWrap">
			<div class="Grid">
				<ul class="PaginatorByIcon-icons"></ul>
			</div>
		</div>
		<div class="PaginatorByIcon-content VideoBackground VideoBackground--tall VideoBackground--wide">
			<div class="VideoBackground-container"></div>

			<div id="prueba" class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="maiev" data-mp4-url="/assets/wow/static/images/maiev.mp4" data-webm-url="/assets/wow/static/images/maiev.webm" data-background-img-url="/assets/wow/static/images/maiev-still.jpg" data-responsive-img-url="/assets/wow/static/images/maiev-compact.png" data-thumbnail-url="/assets/wow/static/images/maiev-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/maiev-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Maiev Cantosombrío</h3>
							<p class="PaginatorByIcon-summary">Si los Vigías son infames entre los elfos de la noche, Maiev Cantosombrío es una leyenda entre ellos. Famosa por su extraordinaria habilidad para capturar cualquier tipo de enemigo, siguió el rastro de Illidan Tempestira, el Traidor, hasta el Templo Oscuro, en Terrallende. Tras la derrota de Illidan, Maiev apresó a los seguidores que sobrevivieron, los Illidari, y juró que nunca permitiría que los vilificados cazadores de demonios volvieran a campar a sus anchas.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="xavius" data-mp4-url="/assets/wow/static/images/xavius.mp4" data-webm-url="/assets/wow/static/images/xavius.webm" data-background-img-url="/assets/wow/static/images/xavius-still.jpg" data-responsive-img-url="/assets/wow/static/images/xavius-compact.png" data-thumbnail-url="/assets/wow/static/images/xavius-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/xavius-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Xavius</h3>
							<p class="PaginatorByIcon-summary">Tiempo ha, Xavius aterrorizó Azeroth de tal manera que casi hizo añicos los muros que separan la realidad de los sueños. Sus maquinaciones terminaron con su derrota, pero se le ha dado otra oportunidad para vengarse. Ahora, Xavius lidera la conquista de Val’sharah, donde el contaminado Árbol del Mundo Shaladrassil extiende la corrupción de la Pesadilla Esmeralda. Con un ejército de sátiros viles a sus órdenes, Xavius no parará hasta aplastar a todo aquel que se oponga a la Legión.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="anduin" data-mp4-url="/assets/wow/static/images/anduin.mp4" data-webm-url="/assets/wow/static/images/anduin.webm" data-background-img-url="/assets/wow/static/images/anduin-still.jpg" data-responsive-img-url="/assets/wow/static/images/anduin-compact.png" data-thumbnail-url="/assets/wow/static/images/anduin-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/anduin-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Anduin Wrynn</h3>
							<p class="PaginatorByIcon-summary">Anduin, heredero del trono de Ventormenta, es un joven muy sabio para su edad. Son varias las veces que ha recurrido a la diplomacia para poner fin a los conflictos, y ha inspirado hasta a su aguerrido padre para deponer las armas en aras de la paz. No obstante, la diplomacia tiene un límite, y hay villanos con los que no se puede razonar. La Legión Ardiente amenaza con aniquilar Azeroth, y Anduin descubrirá el auténtico precio de la paz… y si está dispuesto a pagarlo.</p>
							<p>
								<a class="Button" href="http://battle.net/wow/game/lore/characters/anduin-wrynn" data-analytics="wow7-action" data-analytics-placement="Character Page - anduin">
									<div class="Button-left"></div>
									<div class="Button-middle"></div>
									<div class="Button-right"></div>
									<div class="Button-light"></div>
									<div class="Button-text">Más información</div>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="athissa" data-mp4-url="/assets/wow/static/images/athissa.mp4" data-webm-url="/assets/wow/static/images/athissa.webm" data-background-img-url="/assets/wow/static/images/athissa-still.jpg" data-responsive-img-url="/assets/wow/static/images/athissa-compact.png" data-thumbnail-url="/assets/wow/static/images/athissa-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/athissa-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Maestra de las mareas Athissa</h3>
							<p class="PaginatorByIcon-summary">La reina Azshara, antigua líder de los ancestrales elfos de la noche, gobierna a los naga con autoridad suprema. Sus siervos se cuentan por millares, pero, de entre ellos, pocos son tan fanáticos como Athissa. Su Reina la designó para dirigir un enorme ejército naga hacia Azsuna, donde se rumorea que hay una reliquia de poder de los titanes, largo tiempo perdida. Nada se interpondrá entre Athissa y su objetivo, ni los espíritus malditos de los elfos de la noche que deambulan por esa región ni las tropas de la Horda y la Alianza que se concentran en las Islas Abruptas.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="genn" data-mp4-url="/assets/wow/static/images/genn.mp4" data-webm-url="/assets/wow/static/images/genn.webm" data-background-img-url="/assets/wow/static/images/genn-still.jpg" data-responsive-img-url="/assets/wow/static/images/genn-compact.png" data-thumbnail-url="/assets/wow/static/images/genn-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/genn-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Genn Cringris</h3>
							<p class="PaginatorByIcon-summary">Durante los últimos años, el veterano gobernador de Gilneas ha sufrido muchas desgracias: su hijo y su nación cayeron ante la Reina alma en pena Sylvanas y los Renegados, y casi perdió su humanidad a causa de la maldición huargen. Sin embargo, a pesar de todas estas tragedias, Genn ha hallado fuerza y coraje, y está decidido a luchar con uñas y dientes contra cualquier enemigo que amenace a la Alianza.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="dargrul" data-mp4-url="/assets/wow/static/images/dargrul.mp4" data-webm-url="/assets/wow/static/images/dargrul.webm" data-background-img-url="/assets/wow/static/images/dargrul-still.jpg" data-responsive-img-url="/assets/wow/static/images/dargrul-compact.png" data-thumbnail-url="/assets/wow/static/images/dargrul-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/dargrul-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Dargrul el Infrarrey</h3>
							<p class="PaginatorByIcon-summary">Durante siglos, las tribus tauren y drógbar de Monte Alto vivieron en paz, pero poco después de la llegada de la Legión, un líder drógbar llamado Dargrul el Infrarrey acabó con la armonía y robó el Martillo de Khaz’goroth, un poderoso artefacto protegido por los tauren Monte Alto. El Infrarrey piensa servirse del terrible poder del Martillo para tomar el control de Monte Alto.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="khadgar" data-mp4-url="/assets/wow/static/images/khadgar.mp4" data-webm-url="/assets/wow/static/images/khadgar.webm" data-background-img-url="/assets/wow/static/images/khadgar-still.jpg" data-responsive-img-url="/assets/wow/static/images/khadgar-compact.png" data-thumbnail-url="/assets/wow/static/images/khadgar-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/khadgar-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Khadgar</h3>
							<p class="PaginatorByIcon-summary">A pesar de que no pudo impedir que Gul’dan abriera la Tumba de Sargeras y la Legión volviera a irrumpir en Azeroth, Khadgar está decidido a hallar el modo de volver a sellar el portal y frustrar la invasión de la Legión. Sabe que, para conseguirlo, necesitará unir a los mayores campeones de Azeroth bajo la causa y hacer frente a aquellos que se oponen a formar una coalición, arriesgándose a acabar destruidos a manos de los demonios.</p>
							<p>
								<a class="Button" href="http://battle.net/wow/game/lore/characters/khadgar" data-analytics="wow7-action" data-analytics-placement="Character Page - khadgar">
									<div class="Button-left"></div>
									<div class="Button-middle"></div>
									<div class="Button-right"></div>
									<div class="Button-light"></div>
									<div class="Button-text">Más información</div>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="guldan" data-mp4-url="/assets/wow/static/images/guldan.mp4" data-webm-url="/assets/wow/static/images/guldan.webm" data-background-img-url="/assets/wow/static/images/guldan-still.jpg" data-responsive-img-url="/assets/wow/static/images/guldan-compact.png" data-thumbnail-url="/assets/wow/static/images/guldan-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/guldan-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Gul'dan</h3>
							<p class="PaginatorByIcon-summary">Gul'dan no debe lealtad a nadie salvo a sus amos de la Legión Ardiente. En Draenor, el ambicioso brujo orco estuvo a punto de someter a toda su raza bajo el yugo de los demonios. Aunque sus planes fracasaron, Gul'dan sobrevivió, y la Legión lo exilió a Azeroth, desde donde abriría un portal que permitiría la entrada de un monstruoso ejército invasor, algo a lo que ni la Horda ni la Alianza se han enfrentado jamás.</p>
							<p>
								<a class="Button" href="http://battle.net/wow/game/lore/characters/guldan" data-analytics="wow7-action" data-analytics-placement="Character Page - guldan">
									<div class="Button-left"></div>
									<div class="Button-middle"></div>
									<div class="Button-right"></div>
									<div class="Button-light"></div>
									<div class="Button-text">Más información</div>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="sylvanas" data-mp4-url="/assets/wow/static/images/sylvanas.mp4" data-webm-url="/assets/wow/static/images/sylvanas.webm" data-background-img-url="/assets/wow/static/images/sylvanas-still.jpg" data-responsive-img-url="/assets/wow/static/images/sylvanas-compact.png" data-thumbnail-url="/assets/wow/static/images/sylvanas-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/sylvanas-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Sylvanas Brisaveloz</h3>
							<p class="PaginatorByIcon-summary">La despiadada líder de los Renegados es una formidable campeona de su pueblo, pero, con la invasión de la Legión Ardiente, la Dama Oscura corre un riesgo extremo: si Sylvanas cae, su muerte será el comienzo de su eterna condena. Lo único que la separa de su fatal destino son sus Val'kyr, aunque son pocos los espíritus guardianes que quedan para apoyarla. Su destino pende de un hilo, y Sylvanas debe decidir hasta qué punto está dispuesta a proteger a su pueblo… y qué es lo más valioso para ella: su gente o su propia alma.</p>
							<p>
								<a class="Button" href="http://battle.net/wow/game/lore/characters/sylvanas-windrunner" data-analytics="wow7-action" data-analytics-placement="Character Page - sylvanas">
									<div class="Button-left"></div>
									<div class="Button-middle"></div>
									<div class="Button-right"></div>
									<div class="Button-light"></div>
									<div class="Button-text">Más información</div>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="PaginatorByIcon-contentItem" data-item-type="character" data-slug="illidan" data-mp4-url="/assets/wow/static/images/illidan.mp4" data-webm-url="/assets/wow/static/images/illidan.webm" data-background-img-url="/assets/wow/static/images/illidan-still.jpg" data-responsive-img-url="/assets/wow/static/images/illidan-compact.png" data-thumbnail-url="/assets/wow/static/images/illidan-portrait.jpg">
				<div class="Grid">
					<div class="Grid-cell Grid-cell--1of2 Wide-hide"></div>
					<div class="Grid-cell Grid-cell--1of2 Wide-cell--1of1">
						<img class="VideoBackground-responsiveImg" src="/assets/wow/static/images/illidan-compact.png" />
						<div class="tempClass-addPadding">
							<h3 class="PaginatorByIcon-header">Illidan Tempestira</h3>
							<p class="PaginatorByIcon-summary">Tras la caída del Templo Oscuro, el cadáver de Illidan Tempestira, el señor de Terrallende, desapareció. Nadie sabe qué fue de los restos de Illidan, pero la leyenda dice que la celadora Maiev Cantosombrío trasladó su cuerpo destrozado a la Cámara de los Celadores para que el alma oscura y persistente de Illidan sufriera el resto de su eterna sentencia, junto a sus seguidores, los temibles Illidari. Una justicia… perpetua.</p>
							<p>
								<a class="Button" href="http://battle.net/wow/game/lore/characters/illidan-stormrage" data-analytics="wow7-action" data-analytics-placement="Character Page - illidan">
									<div class="Button-left"></div>
									<div class="Button-middle"></div>
									<div class="Button-right"></div>
									<div class="Button-light"></div>
									<div class="Button-text">Más información</div>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="PaginatorByIcon-controls">
			<div class="PaginatorByIcon-prev"></div>
			<div class="PaginatorByIcon-next"></div>
		</div>
	</div>
</div>
<div class="Section Section--bigDividerOne" id="media" data-nav="Medios">
	<script type="text/javascript">
		//<![CDATA[
		var mediaStrings = {
			filters: {
				artwork: "Ilustraciones",
				audio: "Audio",
				comics: "Cómics",
				featured: "Destacados",
				screenshots: "Capturas de pantalla",
				videos: "Vídeos",
				wallpapers: "Fondos de pantalla"
			},
			types: {
				artwork: "Ilustración",
				audio: "Audio",
				comic: "Cómic",
				screenshot: "Captura de pantalla",
				video: "Vídeo",
				wallpaper: "Fondo de pantalla"
			},
			sizes: {
				wide: "Grande",
				standard: "Normal",
				mobile: "Móvil",
				tablet: "Tableta",
				facebook: "Facebook",
				twitter: "Twitter"
			}
		};
		//]]>
	</script>
	<div class="Grid">
		<h2>Medios</h2>
		<h1>Prepárate para la invasión</h1>
	</div>
	<div data-media="">
		<div class="MediaObject MediaObject--audio " data-media-type="audio" data-media-title="La Tumba de Sargeras - Primera parte" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/L79X3F11WCKC1469831615932.jpg" data-media-youtube-id="rY4W0ZzdHes" data-media-youtube-playlist-id="" data-media-description="Los esfuerzos de Gul’dan de corromper la Horda de Hierro en Draenor han fracasado. La Legión Ardiente le ha dado una última oportunidad para redimirse en la Tumba de Sargeras… pero ya se ha cansado de que lo tengan atado tan corto." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/content_folder_media/YDV9UI7JB24A1470073324897.mp3" data-media-download-label="Descargar audio" data-media-download-file-info="MP3 (60,00 MB)" data-media-story-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/GGQL3CODMZ9D1469831832108.pdf" data-media-story-label="Descargar relato" data-media-story-file-info="PDF (1,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--audio " data-media-type="audio" data-media-title="La Tumba de Sargeras - Segunda parte" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/S1Z3NPB8XI421469831616605.jpg" data-media-youtube-id="6dDTUVWaR84" data-media-youtube-playlist-id="" data-media-description="Khadgar ha perseguido a Gul’dan de Draenor a Azeroth, siguiendo su rastro hasta las Islas Abruptas. Allí se encontrará con enemigos, pero puede que sean sus aliados los que le den más problemas." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/content_folder_media/P4R185QXBOWJ1470073582646.mp3" data-media-download-label="Descargar audio" data-media-download-file-info="MP3 (60,00 MB)" data-media-story-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/ZX1J2UHP6J2X1469831832675.pdf" data-media-story-label="Descargar relato" data-media-story-file-info="PDF (1,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--audio " data-media-type="audio" data-media-title="La Tumba de Sargeras - Tercera parte" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/YKRQ5YLGI1O11469831617187.jpg" data-media-youtube-id="PDvf1tDA4ko" data-media-youtube-playlist-id="" data-media-description="El combate entre Gul'dan y Khadgar se desata en la tumba de Sargeras. El destino de Azeroth pende de un hilo." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/content_folder_media/WYHF8YGXP4AH1470073763646.mp3" data-media-download-label="Descargar audio" data-media-download-file-info="MP3 (60,00 MB)" data-media-story-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/I8V7LM7Q53II1469831833159.pdf" data-media-story-label="Descargar PDF" data-media-story-file-info="PDF (1,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--audio " data-media-type="audio" data-media-title="La Tumba de Sargeras - Cuarta parte" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/6NEZSXOCK4LN1469831617632.jpg" data-media-youtube-id="MFdb-dU0LUk" data-media-youtube-playlist-id="" data-media-description="Gul'dan ha ganado. Khadgar ha sido derrotado. No obstante, es posible que la Legión Ardiente haya perdido también… El futuro de Azeroth (y los planes de la Legión) dependen del uso que quiera hacer Gul'dan de su poder." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/content_folder_media/XWQ56P5YYMFP1470073926868.mp3" data-media-download-label="Descargar audio" data-media-download-file-info="MP3 (60,00 MB)" data-media-story-file-url="" data-media-story-label="" data-media-story-file-info="" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--video " data-media-type="video" data-media-title="Presagistas: La historia de Gul'dan" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/ck/CKB3Y09Y6DIE1469139609763.jpg" data-media-youtube-id="4Yzj4aNTDT8" data-media-youtube-playlist-id="" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--video " data-media-type="video" data-media-title="Presagistas: La historia de Khadgar" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/cg/CG7AS5Q86BDK1469738177478.jpg" data-media-youtube-id="5QBGoG7xglM" data-media-youtube-playlist-id="" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--video " data-media-type="video" data-media-title="Presagistas: La historia de Illidan" data-media-url="" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/O6B9YSIEXJGC1470251126381.jpg" data-media-youtube-id="2d8-4l1OPaw" data-media-youtube-playlist-id="" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--comic " data-media-type="comic" data-media-title="Corazón de Piedra - Cómic digital" data-media-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/ro/ROD2S4MHX4BV1467064875472.jpg" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/l3/L3M910ODXF6U1467065671850.jpg" data-media-description="Antes de los desastrosos eventos del Cataclismo, Magni Barbabronce, antiguo rey de Forjaz, se sometió a un rito ancestral para comunicarse con la tierra, deseoso de encontrar respuestas y orientación para su pueblo. Sin embargo, esas respuestas no llegaron de la forma esperada. Tras completar el ritual, Magni se hizo uno con la tierra y se transformó en una estatua de diamante en lo más profundo de la capital. Moira, la hija repudiada de Magni, regresó a su hogar tras la muerte de su padre, con el propósito de gobernar Forjaz. Sin embargo, acabó compartiendo el poder con los líderes enanos Falstad Martillo Salvaje y Muradin Barbabronce. Ahora, la fragilidad de esa paz podría romperse y provocar que los rivales enanos avivaran de nuevo sus antiguas rencillas. Además, la noticia de un cambio en el trono podría sumir la ciudad en el caos..." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/X5SRZT2X6OM51467065744394.pdf" data-media-download-label="Descargar cómic" data-media-download-file-info="PDF (13,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--comic " data-media-type="comic" data-media-title="El crepúsculo de Suramar – Cómic digital" data-media-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/U8II22JTGLCX1467841124953.jpg" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/B6JX83NHFAUA1467841124976.jpg" data-media-description="En las profundidades de la ciudad élfica de Suramar, hogar de los nocheterna, el brujo orco Gul'dan lanza un terrorífico ultimátum: ceder la Fuente de la Noche, el origen de su poder, o ver su hogar aplastado bajo el embate de la Legión Ardiente. El mal acecha, y la gran magistrix debe decidir entre confiar en su enemigo o arriesgarse a presentar batalla." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/7E3DPQY2CTGS1467840324901.pdf" data-media-download-label="Descargar cómic" data-media-download-file-info="PDF (13,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--comic " data-media-type="comic" data-media-title="Una Montaña Dividida - Cómic digital" data-media-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/CGYQB5IMCDDU1468446521033.jpg" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/QM46L3W9ATHV1468446521087.jpg" data-media-description="Las tribus de Monte Alto llevan mucho tiempo custodiando el Martillo de Khaz'goroth, y han temido el día en que tuviesen que usarlo para proteger a sus familias y su propia vida. Cuando la Legión invadió las Islas Abruptas, los tauren Monte Alto buscaron el consejo de los espíritus. Un giro del destino del Martillo de Khaz'goroth pondrá a prueba su fe y cuestionará tanto las creencias de sus líderes como todo aquello por lo que han luchado..." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/NPEQ91TRKN0M1468446601240.pdf" data-media-download-label="Descargar cómic" data-media-download-file-info="PDF (13,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--comic " data-media-type="comic" data-media-title="Hijo del lobo - Cómic digital" data-media-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/TXA7Z8FHGU9A1469139758303.jpg" data-media-thumb-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/LXBJY3YELDTE1469139758362.jpg" data-media-description="¿Cuál es el precio de la paz? Esta es la pregunta que Anduin Wrynn debe responder mientras la guerra contra un antigo enemigo empieza de nuevo y enemigos en las sombras amenazan su vida.En “Anduin: Hijo del lobo,” una carta del padre de Anduin, el rey Varian, habla te tiempos aciagos que están al llegar, lo que da pie a que el jóven líder reflexione sobre liderazgo y responsabilidad. Anduin deberá enfrentarse a sus miedos y decidir si puede defender Azeroth sin traicionar sus principios." data-media-download-file-url="https://bnetcmsus-a.akamaihd.net/cms/template_resource/XILPPYT5B4GO1469139781759.pdf" data-media-download-label="Descargar cómic" data-media-download-file-info="PDF (10,00 MB)" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-1.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-1-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-2.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-2-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-3.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-3-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-4.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-4-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-5.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-5-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-6.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-6-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-7.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-7-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-8.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-8-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-9.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-9-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-10.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-10-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-11.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-11-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-12.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-12-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-13.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-13-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-14.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-14-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-15.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-15-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-16.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-16-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-17.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-17-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-18.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-18-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-19.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-19-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--artwork " data-media-type="artwork" data-media-title="Ilustración" data-media-url="/assets/wow/static/images/media-artwork-20.jpg" data-media-thumb-url="/assets/wow/static/images/media-artwork-20-thumb.jpg" ></div>
		<div class="MediaObject MediaObject--screenshot " data-media-type="screenshot" data-media-title="Captura de pantalla" data-media-url="/assets/wow/static/images/media-screenshot-1.jpg" data-media-thumb-url="/assets/wow/static/images/media-screenshot-1-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--screenshot " data-media-type="screenshot" data-media-title="Captura de pantalla" data-media-url="/assets/wow/static/images/media-screenshot-2.jpg" data-media-thumb-url="/assets/wow/static/images/media-screenshot-2-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--screenshot " data-media-type="screenshot" data-media-title="Captura de pantalla" data-media-url="/assets/wow/static/images/media-screenshot-3.jpg" data-media-thumb-url="/assets/wow/static/images/media-screenshot-3-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--screenshot " data-media-type="screenshot" data-media-title="Captura de pantalla" data-media-url="/assets/wow/static/images/media-screenshot-4.jpg" data-media-thumb-url="/assets/wow/static/images/media-screenshot-4-thumb.jpg" data-media-featured="true"></div>
		<div class="MediaObject MediaObject--wallpaper " data-media-type="wallpaper" data-media-title="Fondo de pantalla" data-media-url="/assets/wow/static/images/wallpaper-announcement-full.jpg" data-media-thumb-url="/assets/wow/static/images/wallpaper-announcement-thumb.jpg" data-media-wide-url="/assets/wow/static/images/wallpaper-announcement-wide.jpg" data-media-standard-url="/assets/wow/static/images/wallpaper-announcement-standard.jpg" data-media-mobile-url="/assets/wow/static/images/wallpaper-announcement-mobile.jpg" data-media-tablet-url="/assets/wow/static/images/wallpaper-announcement-tablet.jpg" data-media-facebook-url="" data-media-twitter-url="" data-media-featured="true"></div>
	</div>
	<div class="Grid u-align-center u-marginTop-medium">
		<p>
			<a class="Button Button--textGlow" href="" data-media-load-more="" data-analytics="wow7-action" data-analytics-placement="Load More Media">Ver más</a>
		</p>
	</div>
</div>
<div class="Section Section--bigDividerTwo" id="features" data-nav="Características de la expansión">
	<div class="Grid u-align-center">
		<h2>Características de la expansión</h2>
		<h1>La Legión Ardiente aguarda</h1>
		<p>En <em>World of Warcraft: Legion</em> formarás parte de la vanguardia de élite de Azeroth contra la la amenaza de las tinieblas y cambiará tu experiencia en el juego de muchas maneras. Estamos mejorando el sistema de transfiguración del juego para que puedas coleccionar apariencias de objetos y guardar tus trajes favoritos sin sacrificar el valioso espacio de tu bolsa. También implementaremos un nuevo sistema de JcJ con una progresión, unos talentos y unas recompensas específicas para JcJ, además de facilitar que juegues con tus amigos. Azeroth corre peligro, y luchar junto a tus aliados será más importante que nunca.</p>
	</div>
	<div class="Grid u-marginTop-none">
		<div class="Grid-cell Grid-cell--5of8 Grid-push-cell--3of8 Wide-cell--1of1 u-marginTop-medium">
			<img src="/assets/wow/static/images/legion-box-art.png" alt="" />
		</div>
		<div class="Grid-cell Grid-cell--3of8 Grid-pull-cell--5of8 Wide-cell--1of1 u-marginTop-medium FeaturesListContainer">
			<h3>Características:</h3>
			<ul>
				<li>Nuevo continente: Islas Abruptas</li>
				<li>Nueva clase: cazador de demonios</li>
				<li>Artefactos: armas personalizables que aumentarán su poder a medida que subes de nivel</li>
				<li>Sedes y seguidores específicos de clase</li>
				<li>Mazmorras y bandas completamente nuevas</li>
				<li>Nuevos jefes de mundo</li>
				<li>Nivel máximo aumentado a 110</li>
				<li>Sistema de progreso de JcJ rediseñado</li>
				<li>Sistema de transfiguración mejorado</li>
				<li>Características sociales mejoradas</li>
				<li>Subida de nivel de personaje. ¡Sube a un personaje inmediatamente a nivel 100!</li>
				<li>¡Y más!</li>
			</ul>
		</div>
	</div>
	<div class="Grid u-marginTop-none u-align-center">
		<div class="ButtonList u-align-center u-marginTop-none">
			<ul>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="https://www.battle.net/download/getInstallerForGame?gameProgram=WORLD_OF_WARCRAFT" data-analytics="wow7-beta-sign-up" data-analytics-placement="Bottom">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text">Descargar juego</div>
					</a>
				</li>
				<li class="ButtonList-item">
					<a class="Button Button--fixedWidth" href="https://eu.shop.battle.net/es-es/family/world-of-warcraft" data-analytics="wow7-buy" data-analytics-placement="Bottom Button">
						<div class="Button-left"></div>
						<div class="Button-middle"></div>
						<div class="Button-right"></div>
						<div class="Button-light"></div>
						<div class="Button-text">Jugar</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="Section Section--obeliskFooter Section--bigDividerTwo" id="bottom">
</div>
<!--<div class="Section Section--obeliskFooter Section--bigDividerTwo" id="bottom">
    <div class="tempClass-obeliskFooterBuffer">
        <div class="Grid u-align-center">
            <div class="ContentButtonList">


                <div class="ContentButtonList-item">
                    <a class="ContentButtonList-link FrameSmaller" href="http://www.warcraft.com/" data-analytics="wow7-action" data-analytics-placement="Starter Edition - Bottom">
                        <div class="FrameSmaller-overlay"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--top"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--right"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--left"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
                        <span class="ContentButtonList-content ContentButtonList-content--beginnerTrial">
                <span class="ContentButtonList-subheader">¿Nuevo jugador?</span>
                <span class="ContentButtonList-header">Pruébalo gratis</span>
            </span>
                    </a>
                </div>

                <div class="ContentButtonList-item">
                    <a class="ContentButtonList-link FrameSmaller" href="https://battle.net/account/download/?show=wow" data-analytics="wow7-action" data-analytics-placement="Veteran Trial - Bottom">
                        <div class="FrameSmaller-overlay"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--top"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--right"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--bottom"></div>
                        <div class="FrameSmaller-side FrameSmaller-side--left"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--topLeft"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--topRight"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--bottomRight"></div>
                        <div class="FrameSmaller-corner FrameSmaller-corner--bottomLeft"></div>
                        <span class="ContentButtonList-content ContentButtonList-content--veteranTrial">
                <span class="ContentButtonList-subheader">¿Antiguo jugador?</span>
                <span class="ContentButtonList-header">Regresa a la prueba gratis</span>
            </span>
                    </a>
                </div>

            </div>
        </div>
        <div class="Grid u-align-center u-marginTop-xLarge">
            <div class="Social">
                <p class="Social-intro">Mantente al día sobre World of Warcraft: Legion</p>
                <ul class="Social-items">
                    <li class="Social-item">
                        <a class="Social-link Social-link--facebook" href="http://www.facebook.com/WorldofWarcraft.es" data-slug="facebook" data-analytics="wow7-sns" data-analytics-placement="facebook - Bottom">
                            <span class="Social-label">World of Warcraft en Facebook</span>
                        </a>
                    </li>

                    <li class="Social-item">
                        <a class="Social-link Social-link--twitter" href="http://twitter.com/Warcraft_ES" data-slug="twitter" data-analytics="wow7-sns" data-analytics-placement="twitter - Bottom">
                            <span class="Social-label">World of Warcraft en Twitter</span>
                        </a>
                    </li>

                    <li class="Social-item">
                        <a class="Social-link Social-link--youtube" href="http://www.youtube.com/user/WorldofWarcraftES" data-slug="youtube" data-analytics="wow7-sns" data-analytics-placement="youtube - Bottom">
                            <span class="Social-label">Canal de World of Warcraft</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>





    <div class="NavbarFooter is-region-limited" data-timestamp="1532913898560" data-hash="9b7098329cf96903ef657b87acbeb3ba1489b196" data-region-selection="limited" data-region="eu" data-locale="es-es" >
        <div class="NavbarFooter-overlay"></div>
        <div class="NavbarFooter-selector">
            <div class="NavbarFooter-selectorToggle">
                <div class="NavbarFooter-icon NavbarFooter-selectorToggleIcon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                        <use xlink:href="#Navbar-icon-globe"></use>
                    </svg>
                </div>
                <div class="NavbarFooter-selectorToggleLabel">Español (EU)</div>
                <div class="NavbarFooter-icon NavbarFooter-selectorToggleArrow">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                        <use xlink:href="#Navbar-icon-selector"></use>
                    </svg>
                </div>
            </div>
            <div class="NavbarFooter-selectorDropdown">
                <div class="NavbarFooter-selectorDropdownContainer">
                    <div class="NavbarFooter-selectorCloser">
                        <div class="NavbarFooter-selectorCloserAnchor">
                            <div class="NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-close"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="NavbarFooter-selectorRegions NavbarFooter-selectorSection">
                        <div class="NavbarFooter-selectorLabel">Región</div>
                        <a class="NavbarFooter-selectorRegion  NavbarFooter-selectorOption "  data-id="us" >
                            <div class="NavbarFooter-selectorOptionLabel">América y Asia Suroriental</div>
                            <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-check"></use>
                                </svg>
                            </div>
                        </a>
                        <a class="NavbarFooter-selectorRegion is-active is-selected NavbarFooter-selectorOption "  data-id="eu" >
                            <div class="NavbarFooter-selectorOptionLabel">Europa</div>
                            <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-check"></use>
                                </svg>
                            </div>
                        </a>
                        <a class="NavbarFooter-selectorRegion  NavbarFooter-selectorOption "  data-id="kr" >
                            <div class="NavbarFooter-selectorOptionLabel">Corea</div>
                            <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-check"></use>
                                </svg>
                            </div>
                        </a>
                        <a class="NavbarFooter-selectorRegion  NavbarFooter-selectorOption "  data-id="tw" >
                            <div class="NavbarFooter-selectorOptionLabel">Taiwán</div>
                            <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-check"></use>
                                </svg>
                            </div>
                        </a>
                        <a class="NavbarFooter-selectorRegion  NavbarFooter-selectorOption "  data-id="cn" >
                            <div class="NavbarFooter-selectorOptionLabel">China</div>
                            <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                    <use xlink:href="#Navbar-icon-check"></use>
                                </svg>
                            </div>
                        </a>
                        <div class="NavbarFooter-selectorRegionTick">
                            <div class="NavbarFooter-selectorRegionTickOverlay"></div>
                        </div>
                    </div>
                    <div class="NavbarFooter-selectorLocales NavbarFooter-selectorSection">
                        <div class="NavbarFooter-selectorLabel">Idioma</div>
                        <div class="NavbarFooter-selectorSectionPage  " data-region="us">
                            <div class="NavbarFooter-selectorSectionBlock">
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/en-us/legion/" data-id="en-us" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">English (US)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/es-mx/legion/" data-id="es-mx" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Español (AL)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/pt-br/legion/" data-id="pt-br" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Português (AL)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="NavbarFooter-selectorSectionPage is-open is-active " data-region="eu">
                            <div class="NavbarFooter-selectorSectionBlock">
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/de-de/legion/" data-id="de-de" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Deutsch</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/en-gb/legion/" data-id="en-gb" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">English (EU)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale is-active is-selected NavbarFooter-selectorOption " href="/wow/es-es/legion/" data-id="es-es" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Español (EU)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/fr-fr/legion/" data-id="fr-fr" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Français</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/it-it/legion/" data-id="it-it" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Italiano</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/pt-pt/legion/" data-id="pt-pt" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Português (EU)</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/ru-ru/legion/" data-id="ru-ru" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">Русский</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="NavbarFooter-selectorSectionPage  " data-region="kr">
                            <div class="NavbarFooter-selectorSectionBlock">
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/ko-kr/legion/" data-id="ko-kr" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">한국어</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="NavbarFooter-selectorSectionPage  " data-region="tw">
                            <div class="NavbarFooter-selectorSectionBlock">
                                <a class="NavbarFooter-selectorLocale  NavbarFooter-selectorOption " href="/wow/zh-tw/legion/" data-id="zh-tw" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">繁體中文</div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="NavbarFooter-selectorSectionPage  " data-region="cn">
                            <div class="NavbarFooter-selectorSectionBlock">
                                <a class="NavbarFooter-selectorLocale is-external NavbarFooter-selectorOption " href="http://www.battlenet.com.cn/wow/legion/" data-id="zh-cn" data-alias="" >
                                    <div class="NavbarFooter-selectorOptionLabel">简体中文</div>
                                    <div class="NavbarFooter-selectorOptionIcon NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-external"></use>
                                        </svg>
                                    </div>
                                    <div class="NavbarFooter-selectorOptionCheck NavbarFooter-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" focusable="false" >
                                            <use xlink:href="#Navbar-icon-check"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="NavbarFooter-selectorTick"></div>
                </div>
            </div>
        </div>
        <div class="NavbarFooter-logoContainer"><a href="https://www.blizzard.com/" class="NavbarFooter-logo"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 124 66" class="NavbarFooter-logoUpper"><use xlink:href="#NavbarFooter-blizzard-upper"></use></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 124 66" class="NavbarFooter-logoLower"><use xlink:href="#NavbarFooter-blizzard-lower"></use></svg></a></div><div class="NavbarFooter-links NavbarFooter-mainLinks"><div class="NavbarFooter-linksLeft"><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://careers.blizzard.com/" class="NavbarFooter-anchor" data-id="careers" data-analytics="global-nav" data-analytics-placement="Footer - Careers">Empleos</a></div><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://www.blizzard.com/company/about/" class="NavbarFooter-anchor" data-id="about" data-analytics="global-nav" data-analytics-placement="Footer - About">Información</a></div><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://eu.support.blizzard.com/" class="NavbarFooter-anchor" data-id="support" data-analytics="global-nav" data-analytics-placement="Footer - Support">Asistencia</a></div></div><div class="NavbarFooter-linksRight"><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="http://eu.blizzard.com/company/about/contact.html" class="NavbarFooter-anchor" data-id="contact" data-analytics="global-nav">Contacto</a></div><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://blizzard.gamespress.com/" class="NavbarFooter-anchor" data-id="press" data-analytics="global-nav" data-analytics-placement="Footer - Press">Prensa</a></div><div class="NavbarFooter-link NavbarFooter-mainLink"><a href="https://dev.battle.net/" class="NavbarFooter-anchor" data-id="api" data-analytics="global-nav" data-analytics-placement="Footer - Battle.net API">API</a></div></div></div><div class="NavbarFooter-copyright">©2018 Blizzard Entertainment, Inc. Todos los derechos reservados.</div><div class="NavbarFooter-trademark">Todas las marcas registradas referenciadas son propiedad de sus respectivos dueños.</div><div class="NavbarFooter-links NavbarFooter-subLinks"><div class="NavbarFooter-link NavbarFooter-subLink"><a href="https://www.blizzard.com/company/about/privacy.html" class="NavbarFooter-anchor" data-id="privacy" data-analytics="global-nav" data-analytics-placement="Footer - Privacy">Privacidad</a></div><div class="NavbarFooter-link NavbarFooter-subLink"><a href="https://www.blizzard.com/company/legal/" class="NavbarFooter-anchor" data-id="terms" data-analytics="global-nav" data-analytics-placement="Footer - Terms">Términos</a></div></div>
        <div class="NavbarFooter-legal">
            es-ES

            <div class="NavbarFooter-legalRatings">
                <div class="NavbarFooter-legalRatingWrapper">
                    <a class="NavbarFooter-legalLink" href="http://www.esrb.org/ratings/ratings_guide.jsp">
                        <img class="NavbarFooter-legalRatingDetailImage" src="https://bneteu-a.akamaihd.net/api/legal/static/images//legal/ratings/esrb/en/t.424Bl.png" title="" alt=""/>
                    </a>
                    <div class="NavbarFooter-legalRatingDescriptorsWrapper">
                        <div class="NavbarFooter-esrbDescriptor"
                             title="Depictions of blood or the mutilation of body parts.">
                            Blood and Gore
                        </div>
                        <div class="NavbarFooter-esrbDescriptor"
                             title="Depictions or dialogue involving vulgar antics, including “bathroom” humor.">
                            Crude Humor
                        </div>
                        <div class="NavbarFooter-esrbDescriptor"
                             title="Mild to moderate use of profanity.">
                            Mild Language
                        </div>
                        <div class="NavbarFooter-esrbDescriptor"
                             title="Mild provocative references or materials.">
                            Suggestive Themes
                        </div>
                        <div class="NavbarFooter-esrbDescriptor"
                             title="The consumption of alcoholic beverages.">
                            Use of Alcohol
                        </div>
                        <div class="NavbarFooter-esrbDescriptor"
                             title="Scenes involving aggressive conflict. May contain bloodless dismemberment.">
                            Violence
                        </div>
                        <div class="NavbarFooter-esrbDescriptor">Online Interactions Not Rated by the ESRB</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>-->
<div id="modal" class="modal hide">
	<div class="modal-content Frame Frame--inactive">
		<div class="Frame-overlay"></div>
		<div class="Frame-side Frame-side--top"></div>
		<div class="Frame-side Frame-side--right"></div>
		<div class="Frame-side Frame-side--bottom"></div>
		<div class="Frame-side Frame-side--left"></div>
		<div class="Frame-corner Frame-corner--topLeft"></div>
		<div class="Frame-corner Frame-corner--topRight"></div>
		<div class="Frame-corner Frame-corner--bottomRight"></div>
		<div class="Frame-corner Frame-corner--bottomLeft"></div>
		<div class="modal__media"></div>
		<div class="modal-controls">
			<div class="prev arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
			<div class="next arrow-tab modal-navigation">
				<div class="heroes-arrow"></div>
			</div>
		</div>
		<span class="media-btn close">×</span>
	</div>
</div>
<script type="text/javascript" src="/assets/wow/static/js/navbar.js?v=58-127"></script>
<script type="text/javascript" src="/assets/wow/static/js/seven-oh/dist/seven-oh.min.js?v=127"></script>
<!-- Font tracking script. -->
<script type="text/javascript">
	//<![CDATA[
	new Image().src = "//fast.fonts.net/t/1.css?apiType=css&projectid=23f45f39-cbf3-41cf-912f-e015fde06357";
	//]]>
</script>
<script type="text/javascript" src="//bnetcmsus-a.akamaihd.net/cms/template_resource/wow/js/wow.min.1.js?v=58-127"></script>
</body>
</html>
