<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 2/08/2018
 * Time: 7:51 PM
 */
class Panel extends CI_Controller{


	public function __construct()
	{
		parent::__construct();
		$this->load->model("Auth","A");
		$this->load->model("Characters","C");
		$this->load->model("Web","W");
	}

	public function index(){
		if(isset($this->session->idUsuario)){
			$this->W->obtenerIdAcc();
			$o = $this->C->online();
			$data = array(
				"usuario" => $this->session->username,
				"correo" => $this->session->email,
				"nombreCompleto" => $this->session->nombre,
				"rango" => $this->session->rango,
				"perfilFoto" => (isset($this->session->img)) ? $this->session->img : 'illidan-portrait.jpg',
				"hordaOnline" => $o->onlineH,
				"alizOnline" => $o->onlineA,
				"pandaNuestral" => $o->onlineN,
				"onlinePlayers" => $o->onlineT,
				"discord" => $this->W->getDiscordInfo(),
				"onlineWeb" => $this->W->online()
			);
			$this->load->view("panel/index",$data);
		}else{
			redirect(base_url());
		}
	}

	public function bugtracker(){
		echo json_encode($this->W->get_Bugtracker());
		exit();
	}

	public function salir(){
		session_destroy();
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('idUsuario');
		$this->session->unset_userdata('imagen');
		$this->session->sess_destroy();
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
		redirect(base_url());
	}

	public function guardarReporte(){
		if(isset($_POST["titulo"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"description" => ($this->input->post("html")),
					"title" => $this->input->post("titulo"),
					"author" => $this->session->idAcc,
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->W->registrarWeb("bugtracker",$data));
			}
		}else{
			echo json_encode(false);
		}
	}
	function guardarComentarioReporte(){
		if(isset($_POST["idB"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"idBugtracker" => ($this->input->post("idB")),
					"idAuthor" => $this->session->idAcc,
					"comentario" => $this->input->post("comentario"),
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->W->registrarWeb("comment_bugtracker",$data));
			}
		}else{
			echo json_encode(false);
		}
	}

	function buscarComentariosReportes(){
		if(isset($_GET["id"])){
			echo json_encode($this->W->comentariosBugtracker($this->input->get("id")));
		}
	}


	public function eliminarReporte(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->W->updateWeb("bugtracker",array("close"=>1),array("id"=>$this->input->post("id"))));
				}else{
					json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarPostulacion(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->W->updateWeb("postulacion",array("close"=>1),array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarComentarioReporte(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->W->deleteWeb("comment_bugtracker",array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarComentarioPostulacion(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->W->deleteWeb("comment_postulacion",array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}

			}
		}else{
			echo json_encode(false);
		}
	}

	public function uploads(){
		// Allowed extentions.
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		if(!isset($_FILES["file"]["name"])){
			exit(0);
		}
		// Get filename.
		$temp = explode(".", $_FILES["file"]["name"]);

		// Get extension.
		$extension = end($temp);

		// An image check is being done in the editor but it is best to
		// check that again on the server side.
		// Do not use $_FILES["file"]["type"] as it can be easily forged.
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		//echo $_FILES["file"]["tmp_name"];

		$mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

		if ((($mime == "image/gif")
				|| ($mime == "image/jpeg")
				|| ($mime == "image/pjpeg")
				|| ($mime == "image/x-png")
				|| ($mime == "image/png"))
			&& in_array($extension, $allowedExts)) {
			// Generate new random name.
			$name = sha1(microtime()) . "." . $extension;

			// Save file in the uploads folder.
			if(isset($_POST["perfil"])){
				move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . "/assets/panel/perfil/" . $name);
				if($this->W->updateWeb("account",array("imagen"=>$name),array("id"=>$this->session->idAcc))){
					unlink("assets/panel/perfil/".$this->session->img);
					$this->session->img = $name;
					echo json_encode(true);
					exit(0);
				}
			}else{
				move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . "/assets/panel/uploads/" . $name);
			}
			// Generate response.
			$response = new StdClass;
			$response->link = "/uploads/" . $name;
			echo stripslashes(json_encode($response));
		}
	}

	public function jsonImg(){
		$directory="assets/panel/uploads/";
		$dirint = dir($directory);
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$data = array();
		while (($archivo = $dirint->read()) !== false)
		{
			$temp = explode(".",$archivo);
			// Get extension.
			$extension = end($temp);
			if(in_array($extension, $allowedExts)){
				array_push($data,array("url"=>"/".$directory."/".$archivo,"thumb"=>"/".$directory."/".$archivo));
			}else{
				continue;
			}
		}
		$dirint->close();
		echo json_encode(($data));
	}

	public function guardarPostulacion(){
		if(isset($_POST["titulo"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"description" => ($this->input->post("html")),
					"title" => $this->input->post("titulo"),
					"author" => $this->session->idAcc,
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->W->registrarWeb("postulacion",$data));
			}
		}else{
			echo json_encode(false);
		}
	}


	function guardarComentarioPostulacion(){
		if(isset($_POST["idB"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"idPostulacion" => ($this->input->post("idB")),
					"idAuthor" => $this->session->idAcc,
					"comentario" => $this->input->post("comentario"),
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->W->registrarWeb("comment_postulacion",$data));
			}
		}else{
			echo json_encode(false);
		}
	}


	public function postulacion(){
		echo json_encode($this->W->get_Postulacion());
		exit();
	}

	function buscarComentarioPostulacion(){
		if(isset($_GET["id"])){
			echo json_encode($this->W->comentariosPostulacion($this->input->get("id")));
		}
	}

	public function actualizarInformacionPersonal(){
		if(isset($_POST["idAcc"])){
			if($this->session->idAcc == $this->input->post("idAcc")){
				$password = $this->input->post("password");
				if($password != ""){
					if($this->W->updateWeb("account",
						array(
							"nombre" => $this->input->post("nombre"),
							"facebook" => $this->input->post("facebook"),
							"twitter" => $this->input->post("twitter"),
							"telefono" => $this->input->post("telefono"),
							"password" => $password
						),
						array("id"=>$this->session->idAcc))){

						$sha_pass_hash1 = strtoupper(($this->W->encriptacion($this->session->username,$password)));
						$sha_pass_hash2 = strtoupper(($this->W->encriptacion($this->session->email,$password)));
						$this->A->updateAuth("account",array("sha_pass_hash"=>$sha_pass_hash1,"sessionkey"=>""),array("id"=>$this->session->idUsuario));
						$this->A->updateAuth("battlenet_accounts",array("sha_pass_hash"=>$sha_pass_hash2),array("id"=>$this->session->idUsuario));
						echo json_encode(true);
						exit(0);
					}
				}else{
					if($this->W->updateWeb("account",
						array(
							"nombre" => $this->input->post("nombre"),
							"facebook" => $this->input->post("facebook"),
							"twitter" => $this->input->post("twitter"),
							"telefono" => $this->input->post("telefono")

						),
						array("id"=>$this->session->idAcc))){
						echo json_encode(true);
						exit(0);
					}
				}
			}
		}else{
			echo json_encode(false);
		}
	}

}
