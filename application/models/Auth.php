<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 31/07/2018
 * Time: 8:36 PM
 */

class Auth extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->auth = $this->load->database('auth', TRUE);

		}

	public function registrarAuth($tabla, $data = array()){
		return $this->auth->insert($tabla, $data);
	}

	public function updateAuth($tabla,$data=array(),$where = array()){
		return $this->auth->update($tabla,$data,$where);
	}
	public function deleteAuth($tabla,$data=array()){
		return $this->auth->delete($tabla,$data);
	}



	public function get_UserDate($u,$pass){
		return $this->auth->query("SELECT id,username,email FROM account WHERE username = '{$u}' and sha_pass_hash = '{$pass}'");
	}



	public function get_usuario($email){
		$r = $this->auth->query("SELECT username FROM account where email = '{$email}'");
		return $r->num_rows()>0 ? $r->row("username") : false;
	}




	public function validarExits($id,$u){
		if($id == 1){
			return $this->auth->query("select email from account where email = '{$u}'")->num_rows() > 0 ? true : false;
		}else{
			return $this->auth->query("select username from account where username = '{$u}'")->num_rows() > 0 ? true : false;
		}
	}


	public function getIdAccount($account)
	{
		$r = $this->auth->select('id')
			->where('username', $account)
			->get('account');
		return $r = $r->num_rows() > 0  ? $r->row("id") : false;
	}





}
