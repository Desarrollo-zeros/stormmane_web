<?php

/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 1/08/2018
 * Time: 4:29 PM
 */

class Characters extends  CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->characters = $this->load->database('characters', TRUE);
	}


	public function online(){
		return $this->characters->query("select 
											(SELECT count(online)  as online FROM characters WHERE  race in(1,3,4,7,11,22,25) and online = 1) as onlineA,
											(SELECT count(online) as online FROM characters WHERE  race in(2,4,6,8,10,9,26,28) and online = 1) as onlineH,
											(SELECT count(online) as online FROM characters WHERE race = 24 and online = 1) as onlineN,
											(SELECT count(online) as online FROM characters WHERE online = 1) as onlineT
										from characters limit 1;")->row();
	}

	public function getFaction($race)
	{
		switch ($race)
		{
			case '1':
			case '3':
			case '4':
			case '7':
			case '11':
			case '22':
			case '25':
				return 0; //alianza
				break;
			case '2':
			case '5':
			case '6':
			case '8':
			case '10':
			case '9':
			case '26':
			case '28':
				return 1; //horda
				break;
			case '24':
				return 2;
				break;
		}
	}


	public function facciones($race)
	{
		switch ($race)
		{
			case '1':
				return 'Alliance';
				break;
			case '3':
				return 'Alliance';
				break;
			case '4':
				return 'Alliance';
				break;
			case '7':
				return 'Alliance';
				break;
			case '11':
				return 'Alliance';
				break;
			case '22':
				return 'Alliance';
				break;
			case '25':
				return 'Alliance';
				break;
			case '2':
				return 'Horde';
				break;
			case '5':
				return 'Horde';
				break;
			case '6':
				return 'Horde';
				break;
			case '8':
				return 'Horde';
				break;
			case '10':
				return 'Horde';
				break;
			case '9':
				return 'Horde';
				break;
			case '26':
				return 'Horde';
				break;
			case '28':
				return 'Horde';
				break;
			case '24':
				return 'Neutral';
				break;

		}
	}


}
